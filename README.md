# jupyterlab_widget_test

jupyterlab widget test


## Prerequisites

* JupyterLab

## Installation

```bash
jupyter labextension install jupyterlab_widget_test
```

## Development

To create development environment
```bash
conda update -n base conda
conda create -n widget_test -c conda-forge jupyterlab nodejs cookiecutter -y
activate widget_test
cd %conda_prefix%
mkdir projects && cd projects

cookiecutter https://github.com/jupyterlab/extension-cookiecutter-ts

author_name []: tkim
extension_name [jupyterlab_myextension]: jupyterlab_widget_test
project_short_description [A JupyterLab extension.]: jupyterlab widget test
repository [https://github.com/my_name/jupyterlab_myextension]:

cd jupyterlab_widget_test
jlpm install
jlpm run build
jupyter labextension install

npm install -g yarn
yarn add react
yarn add @types/jquery
```

then, open webstorm

To Build the project
```bash

```

For a development install (requires npm version 4 or later), do the following in the repository directory:

```bash
jlpm install
jlpm run build
jupyter labextension link .
```

To rebuild the package and the JupyterLab app:

```bash
jlpm run build
jupyter lab build
```

in sort, you can do like the below;

```
jlpm run build && jupyter lab build && jupyter lab
```


