import {Token} from "@phosphor/coreutils";
import {DisposableDelegate, IDisposable} from '@phosphor/disposable';
import {CommandPalette} from '@phosphor/widgets';
import {JupyterLab} from '@jupyterlab/application';
import {InstanceLoader} from "../core/instanceloader";
import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";
import {WidgetData, WidgetItem} from "../widget/Store";

/**
 * The cloud palette token.
 */
export const IWidgetsPalette = new Token<IWidgetsPalette>('jupyter.services.widgetspalette');

/**
 * The options for creating a command palette item.
 */
export interface IPaletteItem extends CommandPalette.IItemOptions {
}

const TERMINAL_ICON_CLASS = 'jp-TerminalIcon';

export interface IWidgetsPalette {
    /**
     * The placeholder text of the command palette's search input.
     */
    placeholder: string;

    /**
     * Activate the command palette for user input.
     */
    activate(): void;

    /**
     * Add a command item to the command palette.
     *
     * @param options - The options for creating the command item.
     *
     * @returns A disposable that will remove the item from the palette.
     */
    addItem(options: IPaletteItem): IDisposable;
}

/**
 * A thin wrapper around the `CommandPalette` class to conform with the
 * JupyterLab interface for the application-wide command palette.
 */
class WidgetsPalette implements IWidgetsPalette {
    /**
     * Create a palette instance.
     */
    constructor(palette: CommandPalette) {
        this._palette = palette;
    }

    /**
     * The placeholder text of the command palette's search input.
     */
    set placeholder(placeholder: string) {
        this._palette.inputNode.placeholder = placeholder;
    }

    get placeholder(): string {
        return this._palette.inputNode.placeholder;
    }

    /**
     * Activate the command palette for user input.
     */
    activate(): void {
        this._palette.activate();
    }

    /**
     * Add a command item to the command palette.
     *
     * @param options - The options for creating the command item.
     *
     * @returns A disposable that will remove the item from the palette.
     */
    addItem(options: IPaletteItem): IDisposable {
        let item = this._palette.addItem(options as CommandPalette.IItemOptions);
        return new DisposableDelegate(() => this._palette.removeItem(item));
    }

    private _palette: CommandPalette = null;
}

/**
 * Activate the command palette.
 */
export function activateWidgetsPalette(app: JupyterLab): IWidgetsPalette {

    const {commands, shell} = app;
    const palette = new CommandPalette({commands});

    palette.id = 'widgets-palette';
    palette.title.label = 'Widgets';

    /**
     * Activate the command palette within the app shell (used as a command).
     */
    function activatePalette(): void {
        shell.activateById(palette.id);
        palette.activate();
    }

    /**
     * Hide the command palette within the app shell (used as a command).
     */
    function hidePalette(): void {
        if (!palette.isHidden) {
            app.shell.collapseLeft();
        }
    }

    /**
     * Toggle the command palette within the app shell (used as a command).
     */
    function togglePalette(): void {
        if (palette.isHidden) {
            activatePalette();
        } else {
            hidePalette();
        }
    }

    app.commands.addCommand('widgets-palette:activate', {
        execute: activatePalette,
        label: 'Activate Widgets Palette'
    });
    app.commands.addCommand('widgets-palette:hide', {
        execute: hidePalette,
        label: 'Hide Widgets Palette'
    });
    app.commands.addCommand('widgets-palette:toggle', {
        execute: togglePalette,
        label: 'Toggle Widgets Palette'
    });

    palette.inputNode.placeholder = "SEARCH";


    //////////////////////////////////////////////////////////////////
    // register widget items to palette
    WidgetData.forEach((data:WidgetItem) => {

        // add open widget command1
        let command_create_and_activate_widget = `carme:activate:widget:open:${data.label}`;
        app.commands.addCommand(command_create_and_activate_widget, {
            execute: (args: any) => {
                let widget: AbstractVDomRenderer;
                widget = <AbstractVDomRenderer> InstanceLoader.getInstance(args.widgetName);
                widget.id = args.widgetName;
                widget.title.label = args.label;
                widget.title.closable = true;
                widget.title.iconClass = args.iconClass;
                widget.title.caption = args.caption;
                widget.node.style.overflowY = 'auto';

                if (!widget.isAttached) {
                    app.shell.addToMainArea(widget);
                    app.shell.activateById(widget.id);
                } else {
                    app.shell.activateById(widget.id);
                    app.shell.show();
                }
                widget.onReady();
            },
            mnemonic: data.mnemonic,
            caption: data.caption,
            label:data.label,
            icon: data.icon,
            iconClass: data.iconClass,
            iconLabel: data.iconLabel,
            className: data.className
        });

        /***
         * Add Widget Items
         */
        palette.addItem({
            command: command_create_and_activate_widget,
            category: data.category,
            rank: data.rank,
            args: data as any
        });

    });


    app.shell.addToLeftArea(palette);

    console.log('JupyterLab extension activateWidgetsPalette is activated!');

    return new WidgetsPalette(palette);

    // return Promise.resolve(void 0);
}
