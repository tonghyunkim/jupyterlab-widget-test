
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget013 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_pagination.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Pagination</h2>
                <p>The .pagination class provides pagination links:</p>
                <ul className="pagination">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                </ul>
            </div>

            <div className="container">
                <h2>Pagination - Active State</h2>
                <p>Add class .active to let the user know which page he/she is on:</p>
                <ul className="pagination">
                    <li><a href="#">1</a></li>
                    <li className="active"><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                </ul>
            </div>

            <div className="container">
                <h2>Pagination - Disabled State</h2>
                <p>Add class .disabled if a page for some reason is disabled:</p>
                <ul className="pagination">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li className="disabled"><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                </ul>
            </div>

            <div className="container">
                <h2>Pagination - Sizing</h2>
                <p>Add class .pagination-lg for larger blocks or .pagination-sm for smaller blocks.</p>

                <p>Large:</p>
                <ul className="pagination pagination-lg">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                </ul>

                <p>Default:</p>
                <ul className="pagination pagination">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                </ul>

                <p>Small:</p>
                <ul className="pagination pagination-sm">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                </ul>
            </div>

            <div className="container">
                <h2>Breadcrumbs</h2>
                <p>The .breadcrumb class indicates the current page's location within a navigational hierarchy:</p>
                <ul className="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Private</a></li>
                    <li><a href="#">Pictures</a></li>
                    <li className="active">Vacation</li>
                </ul>
            </div>


            
        </div>;
    }
    public onReady(): void {
    }
}