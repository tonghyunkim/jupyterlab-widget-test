
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget010 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_glyphicons.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Glyphicon Examples</h2>
                <p>Envelope icon: <span className="glyphicon glyphicon-envelope"></span></p>
                <p>Envelope icon as a link:
                    <a href="#"><span className="glyphicon glyphicon-envelope"></span></a>
                </p>
                <p>Search icon: <span className="glyphicon glyphicon-search"></span></p>
                <p>Search icon on a button:
                    <button type="button" className="btn btn-default">
                        <span className="glyphicon glyphicon-search"></span> Search
                    </button>
                </p>
                <p>Search icon on a styled button:
                    <button type="button" className="btn btn-info">
                        <span className="glyphicon glyphicon-search"></span> Search
                    </button>
                </p>
                <p>Print icon: <span className="glyphicon glyphicon-print"></span></p>
                <p>Print icon on a styled link button:
                    <a href="#" className="btn btn-success btn-lg">
                        <span className="glyphicon glyphicon-print"></span> Print
                    </a>
                </p>
            </div>
            
        </div>;
    }
    public onReady(): void {
    }
}