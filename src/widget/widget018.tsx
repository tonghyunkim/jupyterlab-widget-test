
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget018 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_collapse.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Simple Collapsible</h2>
                <p>Click on the button to toggle between showing and hiding content.</p>
                <button type="button" className="btn btn-info" data-toggle="collapse" data-target="#demo">Simple
                    collapsible
                </button>
                <div id="demo" className="collapse">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </div>
            </div>

            <div className="container">
                <h2>Simple Collapsible</h2>
                <a href="#demo" className="btn btn-info" data-toggle="collapse">Simple collapsible</a>
                <div id="demo" className="collapse">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </div>
            </div>

            <div className="container">
                <h2>Simple Collapsible</h2>
                <p>Click on the button to toggle between showing and hiding content.</p>
                <button type="button" className="btn btn-info" data-toggle="collapse" data-target="#demo">Simple
                    collapsible
                </button>
                <div id="demo" className="collapse in">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </div>
            </div>

            <div className="container">
                <h2>Collapsible Panel</h2>
                <p>Click on the collapsible panel to open and close it.</p>
                <div className="panel-group">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h4 className="panel-title">
                                <a data-toggle="collapse" href="#collapse1">Collapsible panel</a>
                            </h4>
                        </div>
                        <div id="collapse1" className="panel-collapse collapse">
                            <div className="panel-body">Panel Body</div>
                            <div className="panel-footer">Panel Footer</div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="container">
                <h2>Collapsible List Group</h2>
                <p>Click on the collapsible panel to open and close it.</p>
                <div className="panel-group">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h4 className="panel-title">
                                <a data-toggle="collapse" href="#collapse1">Collapsible list group</a>
                            </h4>
                        </div>
                        <div id="collapse1" className="panel-collapse collapse">
                            <ul className="list-group">
                                <li className="list-group-item">One</li>
                                <li className="list-group-item">Two</li>
                                <li className="list-group-item">Three</li>
                            </ul>
                            <div className="panel-footer">Footer</div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="container">
                <h2>Accordion Example</h2>
                <p><strong>Note:</strong> The <strong>data-parent</strong> attribute makes sure that all collapsible
                    elements under the specified parent will be closed when one of the collapsible item is shown.</p>
                <div className="panel-group" id="accordion">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h4 className="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Collapsible Group
                                    1</a>
                            </h4>
                        </div>
                        <div id="collapse1" className="panel-collapse collapse in">
                            <div className="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h4 className="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Collapsible Group
                                    2</a>
                            </h4>
                        </div>
                        <div id="collapse2" className="panel-collapse collapse">
                            <div className="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h4 className="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Collapsible Group
                                    3</a>
                            </h4>
                        </div>
                        <div id="collapse3" className="panel-collapse collapse">
                            <div className="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>;
    }
    public onReady(): void {
    }
}