
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget023 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_forms_inputs2.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Horizontal form with static control</h2>
                <form className="form-horizontal" action="/action_page.php">
                    <div className="form-group">
                        <label className="control-label col-sm-2" htmlFor="email">Email:</label>
                        <div className="col-sm-10">
                            <p className="form-control-static">someone@example.com</p>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="control-label col-sm-2" htmlFor="pwd">Password:</label>
                        <div className="col-sm-10">
                            <input type="password" className="form-control" id="pwd" placeholder="Enter password"
                                   name="pwd"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-10">
                            <button type="submit" className="btn btn-default">Submit</button>
                        </div>
                    </div>
                </form>
            </div>

            <hr/>

            <div className="container">
                <h3>Input Groups</h3>
                <p>The .input-group class is a container to enhance an input by adding an icon, text or a button in
                    front or behind it as a "help text".</p>
                <p>The .input-group-addon class attaches an icon or help text next to the input field.</p>

                <form>
                    <div className="input-group">
                        <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                        <input id="email" type="text" className="form-control" name="email" placeholder="Email"/>
                    </div>
                    <div className="input-group">
                        <span className="input-group-addon"><i className="glyphicon glyphicon-lock"></i></span>
                        <input id="password" type="password" className="form-control" name="password"
                               placeholder="Password"/>
                    </div>
                    <br/>
                        <div className="input-group">
                            <span className="input-group-addon">Text</span>
                            <input id="msg" type="text" className="form-control" name="msg"
                                   placeholder="Additional Info"/>
                        </div>
                </form>
                <br/>

                    <p>It can also be used on the right side of the input:</p>
                    <form>
                        <div className="input-group">
                            <input id="email" type="text" className="form-control" name="email" placeholder="Email"/>
                                <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                        </div>
                        <div className="input-group">
                            <input id="password" type="password" className="form-control" name="password"
                                   placeholder="Password"/>
                                <span className="input-group-addon"><i className="glyphicon glyphicon-lock"></i></span>
                        </div>
                    </form>
            </div>

            <hr/>

            <div className="container">
                <h1>Input Group Button</h1>
                <p>The .input-group class is a container to enhance an input by adding an icon, text or a button in
                    front or behind it as a "help text".</p>
                <p>The .input-group-btn class attaches a button next to an input field. This is often used as a search
                    bar:</p>
                <form action="/action_page.php">
                    <div className="input-group">
                        <input type="text" className="form-control" placeholder="Search" name="search"/>
                            <div className="input-group-btn">
                                <button className="btn btn-default" type="submit"><i
                                    className="glyphicon glyphicon-search"></i></button>
                            </div>
                    </div>
                </form>
            </div>

            <hr/>

            <div className="container">
                <h2>Horizontal form: control states</h2>
                <form className="form-horizontal">
                    <div className="form-group">
                        <label className="col-sm-2 control-label">Focused</label>
                        <div className="col-sm-10">
                            <input className="form-control" id="focusedInput" type="text" value="Click to focus..."/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="disabledInput" className="col-sm-2 control-label">Disabled</label>
                        <div className="col-sm-10">
                            <input className="form-control" id="disabledInput" type="text"
                                   placeholder="Disabled input here..." disabled/>
                        </div>
                    </div>
                    <fieldset disabled>
                        <div className="form-group">
                            <label htmlFor="disabledTextInput" className="col-sm-2 control-label">Disabled input and
                                select list (Fieldset disabled)</label>
                            <div className="col-sm-10">
                                <input type="text" id="disabledTextInput" className="form-control"
                                       placeholder="Disabled input"/>
                            </div>
                        </div>
                        <div className="form-group">
                            <label htmlFor="disabledSelect" className="col-sm-2 control-label"></label>
                            <div className="col-sm-10">
                                <select id="disabledSelect" className="form-control">
                                    <option>Disabled select</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                    <div className="form-group has-success has-feedback">
                        <label className="col-sm-2 control-label" htmlFor="inputSuccess">Input with success and
                            glyphicon</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control" id="inputSuccess"/>
                                <span className="glyphicon glyphicon-ok form-control-feedback"></span>
                        </div>
                    </div>
                    <div className="form-group has-warning has-feedback">
                        <label className="col-sm-2 control-label" htmlFor="inputWarning">Input with warning and
                            glyphicon</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control" id="inputWarning"/>
                                <span className="glyphicon glyphicon-warning-sign form-control-feedback"></span>
                        </div>
                    </div>
                    <div className="form-group has-error has-feedback">
                        <label className="col-sm-2 control-label" htmlFor="inputError">Input with error and
                            glyphicon</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control" id="inputError"/>
                                <span className="glyphicon glyphicon-remove form-control-feedback"></span>
                        </div>
                    </div>
                </form>
            </div>

            <hr/>

            <div className="container">
                <h2>Inline form: control states</h2>
                <form className="form-inline">
                    <div className="form-group">
                        <label htmlFor="focusedInput">Focused</label>
                        <input className="form-control" id="focusedInput" type="text"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="inputPassword">Disabled</label>
                        <input className="form-control" id="disabledInput" type="text" disabled/>
                    </div>
                    <div className="form-group has-success has-feedback">
                        <label htmlFor="inputSuccess2">Input with success</label>
                        <input type="text" className="form-control" id="inputSuccess2"/>
                            <span className="glyphicon glyphicon-ok form-control-feedback"></span>
                    </div>
                    <div className="form-group has-warning has-feedback">
                        <label htmlFor="inputWarning2">Input with warning</label>
                        <input type="text" className="form-control" id="inputWarning2"/>
                            <span className="glyphicon glyphicon-warning-sign form-control-feedback"></span>
                    </div>
                    <div className="form-group has-error has-feedback">
                        <label htmlFor="inputError2">Input with error</label>
                        <input type="text" className="form-control" id="inputError2"/>
                            <span className="glyphicon glyphicon-remove form-control-feedback"></span>
                    </div>
                </form>
            </div>

        </div>;
    }
    public onReady(): void {
    }
}