
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget022 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_forms_inputs.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Form control: input</h2>
                <p>The form below contains two input elements; one of type text and one of type password:</p>
                <form>
                    <div className="form-group">
                        <label htmlFor="usr">Name:</label>
                        <input type="text" className="form-control" id="usr"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="pwd">Password:</label>
                        <input type="password" className="form-control" id="pwd"/>
                    </div>
                </form>
            </div>

            <hr/>

            <div className="container">
                <h2>Form control: textarea</h2>
                <p>The form below contains a textarea for comments:</p>
                <form>
                    <div className="form-group">
                        <label htmlFor="comment">Comment:</label>
                        <textarea className="form-control" data-rows="5" id="comment"></textarea>
                    </div>
                </form>
            </div>

            <hr/>

            <div className="container">
                <h2>Form control: checkbox</h2>
                <p>The form below contains three checkboxes. The last option is disabled:</p>
                <form>
                    <div className="checkbox">
                        <label><input type="checkbox" value=""/>Option 1</label>
                    </div>
                    <div className="checkbox">
                        <label><input type="checkbox" value=""/>Option 2</label>
                    </div>
                    <div className="checkbox disabled">
                        <label><input type="checkbox" value="" disabled/>Option 3</label>
                    </div>
                </form>
            </div>

            <hr/>

            <div className="container">
                <h2>Form control: inline checkbox</h2>
                <p>The form below contains three inline checkboxes:</p>
                <form>
                    <label className="checkbox-inline">
                        <input type="checkbox" value=""/>Option 1
                    </label>
                    <label className="checkbox-inline">
                        <input type="checkbox" value=""/>Option 2
                    </label>
                    <label className="checkbox-inline">
                        <input type="checkbox" value=""/>Option 3
                    </label>
                </form>
            </div>

            <hr/>

            <div className="container">
                <h2>Form control: radio buttons</h2>
                <p>The form below contains three radio buttons. The last option is disabled:</p>
                <form>
                    <div className="radio">
                        <label><input type="radio" name="optradio"/>Option 1</label>
                    </div>
                    <div className="radio">
                        <label><input type="radio" name="optradio"/>Option 2</label>
                    </div>
                    <div className="radio disabled">
                        <label><input type="radio" name="optradio" disabled/>Option 3</label>
                    </div>
                </form>
            </div>

            <hr/>

            <div className="container">
                <h2>Form control: inline radio buttons</h2>
                <p>The form below contains three inline radio buttons:</p>
                <form>
                    <label className="radio-inline">
                        <input type="radio" name="optradio"/>Option 1
                    </label>
                    <label className="radio-inline">
                        <input type="radio" name="optradio"/>Option 2
                    </label>
                    <label className="radio-inline">
                        <input type="radio" name="optradio"/>Option 3
                    </label>
                </form>
            </div>

            <hr/>

            <div className="container">
                <h2>Form control: select</h2>
                <p>The form below contains two dropdown menus (select lists):</p>
                <form>
                    <div className="form-group">
                        <label htmlFor="sel1">Select list (select one):</label>
                        <select className="form-control" id="sel1">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                        </select>
                        <br/>
                            <label htmlFor="sel2">Mutiple select list (hold shift to select more than one):</label>
                            <select multiple className="form-control" id="sel2">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                    </div>
                </form>
            </div>

            <hr/>
        </div>;
    }
    public onReady(): void {
    }
}