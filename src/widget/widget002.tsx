import * as React from "react";
import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";
import {checkloadjscssfile} from "../index";

/**
 * to build correctly, install these.
 * jlpm add bootstrap
 * jlpm add jquery
 * jlpm add @types/jquery
 * jlpm add popper.js
 */

/**
 * this widget uses bootstrap3
 */
export class Widget002 extends AbstractVDomRenderer {
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>
            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_typography.asp" target="_blank">here</a></div>
            </div>
            <div className="container">
                <h1>h1 Bootstrap heading (36px)</h1>
                <h2>h2 Bootstrap heading (30px)</h2>
                <h3>h3 Bootstrap heading (24px)</h3>
                <h4>h4 Bootstrap heading (18px)</h4>
                <h5>h5 Bootstrap heading (14px)</h5>
                <h6>h6 Bootstrap heading (12px)</h6>
            </div>
            <div className="container">
                <h1>Lighter, Secondary Text</h1>
                <p>The small element is used to create a lighter, secondary text in any heading:</p>
                <h1>h1 heading <small>secondary text</small></h1>
                <h2>h2 heading <small>secondary text</small></h2>
                <h3>h3 heading <small>secondary text</small></h3>
                <h4>h4 heading <small>secondary text</small></h4>
                <h5>h5 heading <small>secondary text</small></h5>
                <h6>h6 heading <small>secondary text</small></h6>
            </div>
            <div className="container">
                <h1>Highlight Text</h1>
                <p>Use the mark element to <mark>highlight</mark> text.</p>
                노란색으로 highlight먹는다.
            </div>
            <div className="container">
                <h1>Abbreviations</h1>
                <p>The abbr element is used to mark up an abbreviation or acronym:</p>
                <p>The <abbr title="World Health Organization">WHO</abbr> was founded in 1948.</p>
            </div>
            <div className="container">
                <h1>Blockquotes</h1>
                <p>The blockquote element is used to present content from another source:</p>
                <blockquote>
                    <p>For 50 years, WWF has been protecting the future of nature. The world's leading conservation
                        organization, WWF works in 100 countries and is supported by 1.2 million members in the United
                        States and close to 5 million globally.</p>
                    <footer>From WWF's website</footer>
                </blockquote>
            </div>
            <div className="container">
                <h1>Blockquotes</h1>
                <p>To show the quote on the right use the class .blockquote-reverse:</p>
                <blockquote className="blockquote-reverse">
                    <p>For 50 years, WWF has been protecting the future of nature. The world's leading conservation
                        organization, WWF works in 100 countries and is supported by 1.2 million members in the United
                        States and close to 5 million globally.</p>
                    <footer>From WWF's website</footer>
                </blockquote>
            </div>
            <div className="container">
                <h1>Description Lists</h1>
                <p>The dl element indicates a description list:</p>
                <dl>
                    <dt>Coffee</dt>
                    <dd>- black hot drink</dd>
                    <dt>Milk</dt>
                    <dd>- white cold drink</dd>
                </dl>
            </div>
            <div className="container">
                <h1>Keyboard Inputs</h1>
                <p>To indicate input that is typically entered via the keyboard, use the kbd element:</p>
                <p>Use <kbd>ctrl + p</kbd> to open the Print dialog box.</p>
            </div>
            <div className="container">
                <h1>Multiple Code Lines</h1>
                <p>For multiple lines of code, use the pre element:</p>
                <pre>{`
Text in a pre element
is displayed in a fixed-width
font, and it preserves
both      spaces and
line breaks.
`}</pre>
            </div>
            <div className="container">
                <h2>Contextual Colors</h2>
                <p>Use the contextual classes to provide "meaning through colors":</p>
                <p className="text-muted">This text is muted.</p>
                <p className="text-primary">This text is important.</p>
                <p className="text-success">This text indicates success.</p>
                <p className="text-info">This text represents some information.</p>
                <p className="text-warning">This text represents a warning.</p>
                <p className="text-danger">This text represents danger.</p>
            </div>
            <div className="container">
                <h2>Contextual Backgrounds</h2>
                <p>Use the contextual background classes to provide "meaning through colors":</p>
                <p className="bg-primary">This text is important.</p>
                <p className="bg-success">This text indicates success.</p>
                <p className="bg-info">This text represents some information.</p>
                <p className="bg-warning">This text represents a warning.</p>
                <p className="bg-danger">This text represents danger.</p>
            </div>
            <div className="container">
                <h2>Typography</h2>
                <p>Use the .lead class to make a paragraph "stand out":</p>
                <p className="lead">This paragraph stands out.</p>
                <p>This is a regular paragraph.</p>
            </div>
        </div>;
    }

    public onReady(): void {

    }
}