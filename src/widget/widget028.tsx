
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget028 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_tooltip.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h3>Tooltip Example</h3>
                <a href="#" data-toggle="tooltip" title="Hooray!">Hover over me</a>
            </div>

            <hr/>

            <div className="container">
                <h3>Tooltip Example</h3>
                <p>The data-placement attribute specifies the tooltip position.</p>
                <ul className="list-inline">
                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Hooray!">Top</a></li>
                    <li><a href="#" data-toggle="tooltip" data-placement="bottom" title="Hooray!">Bottom</a></li>
                    <li><a href="#" data-toggle="tooltip" data-placement="left" title="Hooray!">Left</a></li>
                    <li><a href="#" data-toggle="tooltip" data-placement="right" title="Hooray!">Right</a></li>
                </ul>
            </div>

        </div>;
    }
    public onReady(): void {

        ($('[data-toggle="tooltip"]') as any).tooltip();

    }
}