
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget005 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_jumbotron_header.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <div className="jumbotron">
                    <h1>Bootstrap Tutorial</h1>
                    <p>Bootstrap is the most popular HTML, CSS, and JS framework for developing responsive, mobile-first
                        projects on the web.</p>
                </div>
                <p>This is some text.</p>
                <p>This is another text.</p>
            </div>


            <div className="jumbotron">
                <h1>Bootstrap Tutorial</h1>
                <p>Bootstrap is the most popular HTML, CSS, and JS framework for developing responsive, mobile-first
                    projects on the web.</p>
            </div>

            <div className="container">
                <p>This is some text.</p>
                <p>This is another text.</p>
            </div>

            <div className="container">
                <div className="page-header">
                    <h1>Example Page Header</h1>
                </div>
                <p>This is some text.</p>
                <p>This is another text.</p>
            </div>

        </div>;
    }
    public onReady(): void {
    }
}