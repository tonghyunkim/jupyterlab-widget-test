
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget017 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_dropdowns.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Dropdowns</h2>
                <p>The .dropdown class is used to indicate a dropdown menu.</p>
                <p>Use the .dropdown-menu class to actually build the dropdown menu.</p>
                <p>To open the dropdown menu, use a button or a link with a class of .dropdown-toggle and
                    data-toggle="dropdown".</p>
                <div className="dropdown">
                    <button className="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Dropdown
                        Example
                        <span className="caret"></span></button>
                    <ul className="dropdown-menu">
                        <li><a href="#">HTML</a></li>
                        <li><a href="#">CSS</a></li>
                        <li><a href="#">JavaScript</a></li>
                    </ul>
                </div>
            </div>

            <div className="container">
                <h2>Dropdowns</h2>
                <p>The .divider class is used to separate links inside the dropdown menu with a thin horizontal
                    line:</p>
                <div className="dropdown">
                    <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Tutorials
                        <span className="caret"></span></button>
                    <ul className="dropdown-menu">
                        <li><a href="#">HTML</a></li>
                        <li><a href="#">CSS</a></li>
                        <li><a href="#">JavaScript</a></li>
                        <li className="divider"></li>
                        <li><a href="#">About Us</a></li>
                    </ul>
                </div>
            </div>

            <div className="container">
                <h2>Dropdowns</h2>
                <p>The .dropdown-header class is used to add headers inside the dropdown menu:</p>
                <div className="dropdown">
                    <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Tutorials
                        <span className="caret"></span></button>
                    <ul className="dropdown-menu">
                        <li className="dropdown-header">Dropdown header 1</li>
                        <li><a href="#">HTML</a></li>
                        <li><a href="#">CSS</a></li>
                        <li><a href="#">JavaScript</a></li>
                        <li className="divider"></li>
                        <li className="dropdown-header">Dropdown header 2</li>
                        <li><a href="#">About Us</a></li>
                    </ul>
                </div>
            </div>

            <div className="container">
                <h2>Dropdowns</h2>
                <p>The .active class adds a blue background color to the active link.</p>
                <p>The .disabled class disables a dropdown item (grey text color).</p>
                <div className="dropdown">
                    <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Tutorials
                        <span className="caret"></span></button>
                    <ul className="dropdown-menu">
                        <li><a href="#">Normal</a></li>
                        <li className="disabled"><a href="#">Disabled</a></li>
                        <li className="active"><a href="#">Active</a></li>
                        <li><a href="#">Normal</a></li>
                    </ul>
                </div>
            </div>

            <div className="container">
                <h2>Dropdowns</h2>
                <p>Add the .dropdown-menu-right class to .dropdown-menu to right-align the dropdown menu:</p>
                <div className="dropdown">
                    <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Dropdown
                        Example
                        <span className="caret"></span></button>
                    <ul className="dropdown-menu dropdown-menu-right">
                        <li><a href="#">HTML</a></li>
                        <li><a href="#">CSS</a></li>
                        <li><a href="#">JavaScript</a></li>
                        <li className="divider"></li>
                        <li><a href="#">About Us</a></li>
                    </ul>
                </div>
            </div>

            <div className="container">
                <h2>Dropdowns</h2>
                <p>The .dropup class makes the dropdown menu expand upwards instead of downwards:</p>
                <div className="dropup">
                    <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Dropup
                        Example
                        <span className="caret"></span></button>
                    <ul className="dropdown-menu">
                        <li><a href="#">HTML</a></li>
                        <li><a href="#">CSS</a></li>
                        <li><a href="#">JavaScript</a></li>
                        <li className="divider"></li>
                        <li><a href="#">About Us</a></li>
                    </ul>
                </div>
            </div>

            <div className="container">
                <h2>Dropdowns</h2>
                <p>The .dropdown class is used to indicate a dropdown menu.</p>
                <p>Use the .dropdown-menu class to actually build the dropdown menu.</p>
                <div className="dropdown">
                    <button className="btn btn-default dropdown-toggle" type="button" id="menu1"
                            data-toggle="dropdown">Tutorials
                        <span className="caret"></span></button>
                    <ul className="dropdown-menu" data-role="menu" data-aria-labelledby="menu1">
                        <li data-role="presentation"><a data-role="menuitem" data-tabIndex="-1" href="#">HTML</a></li>
                        <li data-role="presentation"><a data-role="menuitem" data-tabIndex="-1" href="#">CSS</a></li>
                        <li data-role="presentation"><a data-role="menuitem" data-tabIndex="-1" href="#">JavaScript</a></li>
                        <li data-role="presentation" className="divider"></li>
                        <li data-role="presentation"><a data-role="menuitem" data-tabIndex="-1" href="#">About Us</a></li>
                    </ul>
                </div>
            </div>

        </div>;
    }
    public onReady(): void {
    }
}