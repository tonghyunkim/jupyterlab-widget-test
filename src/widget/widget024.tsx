
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget024 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_forms_sizing.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Input Sizing</h2>
                <p>The form below shows input elements with different heights using .input-lg and .input-sm:</p>
                <form>
                    <div className="form-group">
                        <label htmlFor="inputdefault">Default input</label>
                        <input className="form-control" id="inputdefault" type="text"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="inputlg">input-lg</label>
                        <input className="form-control input-lg" id="inputlg" type="text"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="inputsm">input-sm</label>
                        <input className="form-control input-sm" id="inputsm" type="text"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="sel1">Default select list</label>
                        <select className="form-control" id="sel1">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="sel2">input-lg</label>
                        <select className="form-control input-lg" id="sel2">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="sel3">input-sm</label>
                        <select className="form-control input-sm" id="sel3">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                        </select>
                    </div>
                </form>
            </div>

            <hr/>

            <div className="container">
                <h2>Input Sizing</h2>
                <p>You can quickly size labels and form controls within a Horizontal form by adding .form-group-lg or
                    .form-group-sm to the div class="form-group" element:</p>
                <form className="form-horizontal">
                    <div className="form-group form-group-lg">
                        <label className="col-sm-2 control-label" htmlFor="lg">form-group-lg</label>
                        <div className="col-sm-10">
                            <input className="form-control" type="text" id="lg"/>
                        </div>
                    </div>
                    <div className="form-group form-group-sm">
                        <label className="col-sm-2 control-label" htmlFor="sm">form-group-sm</label>
                        <div className="col-sm-10">
                            <input className="form-control" type="text" id="sm"/>
                        </div>
                    </div>
                </form>
            </div>

            <hr/>

            <div className="container">
                <h1>Input Group Size</h1>
                <p>The .input-group class is a container to enhance an input by adding an icon, text or a button in
                    front or behind it as a "help text".</p>
                <p>Use the .input-group-lg if you want a large input group:</p>
                <form>
                    <div className="input-group input-group-lg">
                        <input type="text" className="form-control" placeholder="Search"/>
                            <div className="input-group-btn">
                                <button className="btn btn-default" type="submit"><i
                                    className="glyphicon glyphicon-search"></i></button>
                            </div>
                    </div>
                </form>
                <br/>

                    <p>Use the .input-group-sm if you want a small input group:</p>
                    <form>
                        <div className="input-group input-group-sm">
                            <input type="text" className="form-control" placeholder="Search"/>
                                <div className="input-group-btn">
                                    <button className="btn btn-default" type="submit"><i
                                        className="glyphicon glyphicon-search"></i></button>
                                </div>
                        </div>
                    </form>
            </div>

            <hr/>

            <div className="container">
                <h2>Column Sizing</h2>
                <p>The form below shows input elements with different widths using different .col-xs-* classes:</p>
                <form>
                    <div className="form-group row">
                        <div className="col-xs-2">
                            <label htmlFor="ex1">col-xs-2</label>
                            <input className="form-control" id="ex1" type="text"/>
                        </div>
                        <div className="col-xs-3">
                            <label htmlFor="ex2">col-xs-3</label>
                            <input className="form-control" id="ex2" type="text"/>
                        </div>
                        <div className="col-xs-4">
                            <label htmlFor="ex3">col-xs-4</label>
                            <input className="form-control" id="ex3" type="text"/>
                        </div>
                    </div>
                </form>
            </div>

            <hr/>

            <div className="container">
                <h2>Help text</h2>
                <p>Use the .help-block class to add a block level help text in forms:</p>
                <form>
                    <div className="form-group">
                        <label htmlFor="pwd">Password:</label>
                        <input type="password" className="form-control" id="pwd" placeholder="Enter password"/>
                            <span className="help-block">This is some help text that breaks onto a new line and may extend more than one line.</span>
                    </div>
                    <button type="submit" className="btn btn-default">Submit</button>
                </form>
            </div>

        </div>;
    }
    public onReady(): void {
    }
}