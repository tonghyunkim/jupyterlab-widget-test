
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget014 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_pager.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Pager</h2>
                <p>The .pager class provides previous and next buttons (links):</p>
                <ul className="pager">
                    <li><a href="#">Previous</a></li>
                    <li><a href="#">Next</a></li>
                </ul>
            </div>

            <div className="container">
                <h2>Pager</h2>
                <p>The .previous and .next classes align each link to the sides of the page:</p>
                <ul className="pager">
                    <li className="previous"><a href="#">Previous</a></li>
                    <li className="next"><a href="#">Next</a></li>
                </ul>
            </div>
            
        </div>;
    }
    public onReady(): void {
    }
}