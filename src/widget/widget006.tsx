
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget006 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_wells.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Well</h2>
                <div className="well">Basic Well</div>
            </div>

            <div className="container">
                <h2>Well Size</h2>
                <div className="well well-sm">Small Well</div>
                <div className="well">Normal Well</div>
                <div className="well well-lg">Large Well</div>
            </div>

        </div>;
    }
    public onReady(): void {
    }
}