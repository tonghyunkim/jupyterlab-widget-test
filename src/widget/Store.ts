import {Widget001} from "./widget001";
import {Widget002} from "./widget002";
import {Widget003} from "./widget003";
import {Widget004} from "./widget004";
import {Widget005} from "./widget005";
import {Widget006} from "./widget006";
import {Widget007} from "./widget007";
import {Widget008} from "./widget008";
import {Widget009} from "./widget009";
import {Widget010} from "./widget010";
import {Widget011} from "./widget011";
import {Widget012} from "./widget012";
import {Widget013} from "./widget013";
import {Widget014} from "./widget014";
import {Widget015} from "./widget015";
import {Widget016} from "./widget016";
import {Widget017} from "./widget017";
import {Widget018} from "./widget018";
import {Widget019} from "./widget019";
import {Widget020} from "./widget020";
import {Widget021} from "./widget021";
import {Widget022} from "./widget022";
import {Widget023} from "./widget023";
import {Widget024} from "./widget024";
import {Widget025} from "./widget025";
import {Widget026} from "./widget026";
import {Widget027} from "./widget027";
import {Widget028} from "./widget028";
import {Widget029} from "./widget029";
import {Widget030} from "./widget030";
import {Widget031} from "./widget031";
import {Widget032} from "./widget032";


/**
 * Class Store
 * @type {{Widget001: Widget001}}
 */
export const Store: any = {
    Widget001,
    Widget002,
    Widget003,
    Widget004,
    Widget005,
    Widget006,
    Widget007,
    Widget008,
    Widget009,
    Widget010,
    Widget011,
    Widget012,
    Widget013,
    Widget014,
    Widget015,
    Widget016,
    Widget017,
    Widget018,
    Widget019,
    Widget020,
    Widget021,
    Widget022,
    Widget023,
    Widget024,
    Widget025,
    Widget026,
    Widget027,
    Widget028,
    Widget029,
    Widget030,
    Widget031,
    Widget032
};

export interface WidgetItem {
    widgetName: string
    mnemonic: number
    label: string,
    icon: string,
    iconClass: string,
    iconLabel: string,
    caption: string,
    className: string,
    category: string,
    rank: number
}

const TERMINAL_ICON_CLASS = 'jp-TerminalIcon';

export const WidgetData: WidgetItem[] = [
    {
        widgetName: "Widget001",
        mnemonic: 0,
        label: "BS Grid Basic",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 1
    },
    {
        widgetName: "Widget002",
        mnemonic: 0,
        label: "BS Typography",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 2
    },
    {
        widgetName: "Widget003",
        mnemonic: 0,
        label: "BS Table",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 3
    },
    {
        widgetName: "Widget004",
        mnemonic: 0,
        label: "BS Images",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 4
    },
    {
        widgetName: "Widget005",
        mnemonic: 0,
        label: "BS Jumbotron",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 5
    },
    {
        widgetName: "Widget006",
        mnemonic: 0,
        label: "BS Wells",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 6
    },
    {
        widgetName: "Widget007",
        mnemonic: 0,
        label: "BS Alerts",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 7
    },
    {
        widgetName: "Widget008",
        mnemonic: 0,
        label: "BS Buttons",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 8
    },
    {
        widgetName: "Widget009",
        mnemonic: 0,
        label: "BS Button Groups",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 9
    },
    {
        widgetName: "Widget010",
        mnemonic: 0,
        label: "BS Glyphicons",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 10
    },
    {
        widgetName: "Widget011",
        mnemonic: 0,
        label: "BS Badges/Labels",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 11
    },
    {
        widgetName: "Widget012",
        mnemonic: 0,
        label: "BS Progress Bars",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 12
    },
    {
        widgetName: "Widget013",
        mnemonic: 0,
        label: "BS Pagination",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 13
    },
    {
        widgetName: "Widget014",
        mnemonic: 0,
        label: "BS Pager",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 14
    },
    {
        widgetName: "Widget015",
        mnemonic: 0,
        label: "BS List Groups",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 15
    },
    {
        widgetName: "Widget016",
        mnemonic: 0,
        label: "BS Panels",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 16
    },
    {
        widgetName: "Widget017",
        mnemonic: 0,
        label: "BS Dropdowns",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 17
    },
    {
        widgetName: "Widget018",
        mnemonic: 0,
        label: "BS Collapse",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 18
    },
    {
        widgetName: "Widget019",
        mnemonic: 0,
        label: "BS Tabs/Pills",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 19
    },
    {
        widgetName: "Widget020",
        mnemonic: 0,
        label: "BS Navbar",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 20
    },
    {
        widgetName: "Widget021",
        mnemonic: 0,
        label: "BS Forms",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 21
    },
    {
        widgetName: "Widget022",
        mnemonic: 0,
        label: "BS Inputs",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 22
    },
    {
        widgetName: "Widget023",
        mnemonic: 0,
        label: "BS Inputs 2",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 23
    },
    {
        widgetName: "Widget024",
        mnemonic: 0,
        label: "BS Input Sizing",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 24
    },
    {
        widgetName: "Widget025",
        mnemonic: 0,
        label: "BS Media Objects",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 25
    },
    {
        widgetName: "Widget026",
        mnemonic: 0,
        label: "BS Carousel",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 26
    },
    {
        widgetName: "Widget027",
        mnemonic: 0,
        label: "BS Modal",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 27
    },
    {
        widgetName: "Widget028",
        mnemonic: 0,
        label: "BS Tooltip",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 28
    },
    {
        widgetName: "Widget029",
        mnemonic: 0,
        label: "BS Popover",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 29
    },
    {
        widgetName: "Widget030",
        mnemonic: 0,
        label: "BS Scrollspy",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 30
    },
    {
        widgetName: "Widget031",
        mnemonic: 0,
        label: "BS Affix",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 31
    },
    {
        widgetName: "Widget032",
        mnemonic: 0,
        label: "BS Filters",
        icon: TERMINAL_ICON_CLASS,
        iconClass: null,
        iconLabel: null,
        caption: null,
        className: null,
        category: "Bootstrap Tutorial",
        rank: 32
    }
];

