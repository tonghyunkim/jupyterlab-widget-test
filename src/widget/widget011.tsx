
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget011 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_badges_labels.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Badges</h2>
                <a href="#">News <span className="badge">5</span></a><br/>
                <a href="#">Comments <span className="badge">10</span></a><br/>
                <a href="#">Updates <span className="badge">2</span></a>
            </div>

            <div className="container">
                <h2>Badges on Buttons</h2>
                <button type="button" className="btn btn-primary">Primary <span className="badge">7</span></button>
                <button type="button" className="btn btn-success">Success <span className="badge">3</span></button>
                <button type="button" className="btn btn-danger">Danger <span className="badge">5</span></button>
            </div>

            <div className="container">
                <h2>Labels</h2>
                <h1>Example <span className="label label-default">New</span></h1>
                <h2>Example <span className="label label-default">New</span></h2>
                <h3>Example <span className="label label-default">New</span></h3>
                <h4>Example <span className="label label-default">New</span></h4>
                <h5>Example <span className="label label-default">New</span></h5>
                <h6>Example <span className="label label-default">New</span></h6>
            </div>

            <div className="container">
                <h2>Contextual Label Classes</h2>
                <p>Contextual classes can be used to color the label.</p>
                <span className="label label-default">Default Label</span>
                <span className="label label-primary">Primary Label</span>
                <span className="label label-success">Success Label</span>
                <span className="label label-info">Info Label</span>
                <span className="label label-warning">Warning Label</span>
                <span className="label label-danger">Danger Label</span>
            </div>

        </div>;
    }
    public onReady(): void {
    }
}