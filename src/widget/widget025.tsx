
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget025 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_media_objects.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Media Object</h2>
                <p>Use the "media-left" class to left-align a media object. Text that should appear next to the image,
                    is placed inside a container with class="media-body".</p>
                <p>Tip: Use the "media-right" class to right-align the media object.</p><br/>

                <div className="media">
                    <div className="media-left">
                        <img src="https://www.w3schools.com/bootstrap/img_avatar1.png" className="media-object" data-style="width:60px"/>
                    </div>
                    <div className="media-body">
                        <h4 className="media-heading">Left-aligned</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua.</p>
                    </div>
                </div>
                <hr/>

                    <div className="media">
                        <div className="media-body">
                            <h4 className="media-heading">Right-aligned</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua.</p>
                        </div>
                        <div className="media-right">
                            <img src="https://www.w3schools.com/bootstrap/img_avatar1.png" className="media-object" data-style="width:60px"/>
                        </div>
                    </div>
            </div>

            <hr/>

            <div className="container">
                <h2>Media Object</h2>
                <p>The media object can also be top, middle or bottom-aligned with the "media-top", "media-middle" or
                    "media-bottom" class:</p><br/>
                <div className="media">
                    <div className="media-left media-top">
                        <img src="https://www.w3schools.com/bootstrap/img_avatar1.png" className="media-object" data-style="width:80px"/>
                    </div>
                    <div className="media-body">
                        <h4 className="media-heading">Media Top</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua.</p>
                    </div>
                </div>
                <hr/>
                    <div className="media">
                        <div className="media-left media-middle">
                            <img src="https://www.w3schools.com/bootstrap/img_avatar1.png" className="media-object" data-style="width:80px"/>
                        </div>
                        <div className="media-body">
                            <h4 className="media-heading">Media Middle</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                    <hr/>
                        <div className="media">
                            <div className="media-left media-bottom">
                                <img src="https://www.w3schools.com/bootstrap/img_avatar1.png" className="media-object" data-style="width:80px"/>
                            </div>
                            <div className="media-body">
                                <h4 className="media-heading">Media Bottom</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
            </div>

            <hr/>

            <div className="container">
                <h2>Nested Media Objects</h2>
                <p>Media objects can also be nested (a media object inside a media object):</p><br/>
                <div className="media">
                    <div className="media-left">
                        <img src="https://www.w3schools.com/bootstrap/img_avatar1.png" className="media-object" data-style="width:45px"/>
                    </div>
                    <div className="media-body">
                        <h4 className="media-heading">John Doe <small><i>Posted on February 19, 2016</i></small></h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua.</p>

                        <div className="media">
                            <div className="media-left">
                                <img src="https://www.w3schools.com/bootstrap/img_avatar2.png" className="media-object" data-style="width:45px"/>
                            </div>
                            <div className="media-body">
                                <h4 className="media-heading">John Doe <small><i>Posted on February 19, 2016</i></small>
                                </h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua.</p>

                                <div className="media">
                                    <div className="media-left">
                                        <img src="https://www.w3schools.com/bootstrap/img_avatar3.png" className="media-object" data-style="width:45px"/>
                                    </div>
                                    <div className="media-body">
                                        <h4 className="media-heading">John Doe <small><i>Posted on February 19, 2016</i>
                                        </small></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <hr/>

            <div className="container">
                <h2>Nested Media Objects</h2>
                <p>Media objects can also be nested (a media object inside a media object):</p><br/>
                <div className="media">
                    <div className="media-left">
                        <img src="https://www.w3schools.com/bootstrap/img_avatar1.png" className="media-object" data-style="width:45px"/>
                    </div>
                    <div className="media-body">
                        <h4 className="media-heading">John Doe <small><i>Posted on February 19, 2016</i></small></h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua.</p>

                        <div className="media">
                            <div className="media-left">
                                <img src="https://www.w3schools.com/bootstrap/img_avatar2.png" className="media-object" data-style="width:45px"/>
                            </div>
                            <div className="media-body">
                                <h4 className="media-heading">John Doe <small><i>Posted on February 20, 2016</i></small>
                                </h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua.</p>

                                <div className="media">
                                    <div className="media-left">
                                        <img src="https://www.w3schools.com/bootstrap/img_avatar3.png" className="media-object" data-style="width:45px"/>
                                    </div>
                                    <div className="media-body">
                                        <h4 className="media-heading">John Doe <small><i>Posted on February 21, 2016</i>
                                        </small></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                </div>

                            </div>

                            <div className="media">
                                <div className="media-left">
                                    <img src="https://www.w3schools.com/bootstrap/img_avatar4.png" className="media-object" data-style="width:45px"/>
                                </div>
                                <div className="media-body">
                                    <h4 className="media-heading">Jane Doe <small><i>Posted on February 20, 2016</i>
                                    </small></h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua.</p>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div className="media">
                        <div className="media-left">
                            <img src="https://www.w3schools.com/bootstrap/img_avatar5.png" className="media-object" data-style="width:45px"/>
                        </div>
                        <div className="media-body">
                            <h4 className="media-heading">Jane Doe <small><i>Posted on February 19, 2016</i></small>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>;
    }
    public onReady(): void {
    }
}