
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget004 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_images.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Rounded Corners</h2>
                <p>The .img-rounded class adds rounded corners to an image (not available in IE8):</p>
                <img src="https://www.w3schools.com/bootstrap/cinqueterre.jpg" className="img-rounded" alt="Cinque Terre" width="304" height="236"/>
            </div>

            <div className="container">
                <h2>Circle</h2>
                <p>The .img-circle class shapes the image to a circle (not available in IE8):</p>
                <img src="https://www.w3schools.com/bootstrap/cinqueterre.jpg" className="img-circle" alt="Cinque Terre" width="304" height="236"/>
            </div>

            <div className="container">
                <h2>Thumbnail</h2>
                <p>The .img-thumbnail class creates a thumbnail of the image:</p>
                <img src="https://www.w3schools.com/bootstrap/cinqueterre.jpg" className="img-thumbnail" alt="Cinque Terre" width="304" height="236" />
            </div>

            <div className="container">
                <h2>Image</h2>
                <p>The .img-responsive class makes the image scale nicely to the parent element (resize the browser
                    window to see the effect):</p>
                <img className="https://www.w3schools.com/bootstrap/img-responsive" src="img_chania.jpg" alt="Chania" width="460" height="345" />
            </div>
        </div>;
    }
    public onReady(): void {
    }
}