
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget019 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_tabs_pills.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h3>Inline List</h3>
                <ul className="list-inline">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Menu 1</a></li>
                    <li><a href="#">Menu 2</a></li>
                    <li><a href="#">Menu 3</a></li>
                </ul>
            </div>

            <div className="container">
                <h3>Tabs</h3>
                <ul className="nav nav-tabs">
                    <li className="active"><a href="#">Home</a></li>
                    <li><a href="#">Menu 1</a></li>
                    <li><a href="#">Menu 2</a></li>
                    <li><a href="#">Menu 3</a></li>
                </ul>
                <br/>
                    <p><strong>Note:</strong> This example shows how to create a basic navigation tab. It is not
                        toggleable/dynamic yet (you can't click on the links to display different content)- see the last
                        example in the Bootstrap Tabs and Pills Tutorial to find out how this can be done.</p>
            </div>

            <div className="container">
                <h3>Tabs With Dropdown Menu</h3>
                <ul className="nav nav-tabs">
                    <li className="active"><a href="#">Home</a></li>
                    <li className="dropdown">
                        <a className="dropdown-toggle" data-toggle="dropdown" href="#">Menu 1 <span
                            className="caret"></span></a>
                        <ul className="dropdown-menu">
                            <li><a href="#">Submenu 1-1</a></li>
                            <li><a href="#">Submenu 1-2</a></li>
                            <li><a href="#">Submenu 1-3</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Menu 2</a></li>
                    <li><a href="#">Menu 3</a></li>
                </ul>
            </div>

            <div className="container">
                <h3>Pills</h3>
                <ul className="nav nav-pills">
                    <li className="active"><a href="#">Home</a></li>
                    <li><a href="#">Menu 1</a></li>
                    <li><a href="#">Menu 2</a></li>
                    <li><a href="#">Menu 3</a></li>
                </ul>
            </div>

            <div className="container">
                <h3>Vertical Pills</h3>
                <p>Use the .nav-stacked class to vertically stack pills:</p>
                <ul className="nav nav-pills nav-stacked">
                    <li className="active"><a href="#">Home</a></li>
                    <li><a href="#">Menu 1</a></li>
                    <li><a href="#">Menu 2</a></li>
                    <li><a href="#">Menu 3</a></li>
                </ul>
            </div>

            <div className="container">
                <h3>Vertical Pills</h3>
                <div className="row">
                    <div className="col-md-3">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua.</p>
                    </div>
                    <div className="col-md-3">
                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                            commodo consequat.</p>
                    </div>
                    <div className="col-md-3">
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                            laudantium, totam rem aperiam.</p>
                    </div>
                    <div className="col-md-3">
                        <ul className="nav nav-pills nav-stacked">
                            <li className="active"><a href="#">Home</a></li>
                            <li><a href="#">Menu 1</a></li>
                            <li><a href="#">Menu 2</a></li>
                            <li><a href="#">Menu 3</a></li>
                        </ul>
                    </div>
                    <div className="clearfix visible-lg"></div>
                </div>
            </div>

            <div className="container">
                <h3>Pills With Dropdown Menu</h3>
                <ul className="nav nav-pills nav-stacked">
                    <li className="active"><a href="#">Home</a></li>
                    <li className="dropdown">
                        <a className="dropdown-toggle" data-toggle="dropdown" href="#">Menu 1 <span
                            className="caret"></span></a>
                        <ul className="dropdown-menu">
                            <li><a href="#">Submenu 1-1</a></li>
                            <li><a href="#">Submenu 1-2</a></li>
                            <li><a href="#">Submenu 1-3</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Menu 2</a></li>
                    <li><a href="#">Menu 3</a></li>
                </ul>
            </div>

            <div className="container">
                <h3>Centered Tabs</h3>
                <p>To center/justify the tabs and pills, use the .nav-justified class. Note that on screens that are
                    smaller than 768px, the list items are stacked (content will remain centered).</p>
                <ul className="nav nav-tabs nav-justified">
                    <li className="active"><a href="#">Home</a></li>
                    <li><a href="#">Menu 1</a></li>
                    <li><a href="#">Menu 2</a></li>
                    <li><a href="#">Menu 3</a></li>
                </ul>
                <br/>
                    <h3>Centered Pills</h3>
                    <ul className="nav nav-pills nav-justified">
                        <li className="active"><a href="#">Home</a></li>
                        <li><a href="#">Menu 1</a></li>
                        <li><a href="#">Menu 2</a></li>
                        <li><a href="#">Menu 3</a></li>
                    </ul>
            </div>

            <div className="container">
                <h2>Dynamic Tabs</h2>
                <p>To make the tabs toggleable, add the data-toggle="tab" attribute to each link. Then add a .tab-pane
                    class with a unique ID for every tab and wrap them inside a div element with class .tab-content.</p>

                <ul className="nav nav-tabs">
                    <li className="active"><a data-toggle="tab" href="#home">Home</a></li>
                    <li><a data-toggle="tab" href="#menu1">Menu 1</a></li>
                    <li><a data-toggle="tab" href="#menu2">Menu 2</a></li>
                    <li><a data-toggle="tab" href="#menu3">Menu 3</a></li>
                </ul>

                <div className="tab-content">
                    <div id="home" className="tab-pane fade in active">
                        <h3>HOME</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua.</p>
                    </div>
                    <div id="menu1" className="tab-pane fade">
                        <h3>Menu 1</h3>
                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                            commodo consequat.</p>
                    </div>
                    <div id="menu2" className="tab-pane fade">
                        <h3>Menu 2</h3>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                            laudantium, totam rem aperiam.</p>
                    </div>
                    <div id="menu3" className="tab-pane fade">
                        <h3>Menu 3</h3>
                        <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                            explicabo.</p>
                    </div>
                </div>
            </div>

            <div className="container">
                <h2>Dynamic Pills</h2>
                <p>To make the tabs toggleable, add the data-toggle="pill" attribute to each link. Then add a .tab-pane
                    class with a unique ID for every tab and wrap them inside a div element with class .tab-content.</p>
                <ul className="nav nav-pills">
                    <li className="active"><a data-toggle="pill" href="#home">Home</a></li>
                    <li><a data-toggle="pill" href="#menu1">Menu 1</a></li>
                    <li><a data-toggle="pill" href="#menu2">Menu 2</a></li>
                    <li><a data-toggle="pill" href="#menu3">Menu 3</a></li>
                </ul>

                <div className="tab-content">
                    <div id="home" className="tab-pane fade in active">
                        <h3>HOME</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua.</p>
                    </div>
                    <div id="menu1" className="tab-pane fade">
                        <h3>Menu 1</h3>
                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                            commodo consequat.</p>
                    </div>
                    <div id="menu2" className="tab-pane fade">
                        <h3>Menu 2</h3>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                            laudantium, totam rem aperiam.</p>
                    </div>
                    <div id="menu3" className="tab-pane fade">
                        <h3>Menu 3</h3>
                        <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                            explicabo.</p>
                    </div>
                </div>
            </div>


        </div>;
    }
    public onReady(): void {
    }
}