
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget021 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_forms.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Vertical (basic) form</h2>
                <form action="/action_page.php">
                    <div className="form-group">
                        <label htmlFor="email">Email:</label>
                        <input type="email" className="form-control" id="email" placeholder="Enter email" name="email"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="pwd">Password:</label>
                        <input type="password" className="form-control" id="pwd" placeholder="Enter password"
                               name="pwd"/>
                    </div>
                    <div className="checkbox">
                        <label><input type="checkbox" name="remember"/> Remember me</label>
                    </div>
                    <button type="submit" className="btn btn-default">Submit</button>
                </form>
            </div>

            <hr/>

            <div className="container">
                <h2>Inline form</h2>
                <p>Make the viewport larger than 768px wide to see that all of the form elements are inline, left
                    aligned, and the labels are alongside.</p>
                <form className="form-inline" action="/action_page.php">
                    <div className="form-group">
                        <label htmlFor="email">Email:</label>
                        <input type="email" className="form-control" id="email" placeholder="Enter email" name="email"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="pwd">Password:</label>
                        <input type="password" className="form-control" id="pwd" placeholder="Enter password"
                               name="pwd"/>
                    </div>
                    <div className="checkbox">
                        <label><input type="checkbox" name="remember"/> Remember me</label>
                    </div>
                    <button type="submit" className="btn btn-default">Submit</button>
                </form>
            </div>

            <hr/>

            <div className="container">
                <h2>Inline form with .sr-only class</h2>
                <p>Make the viewport larger than 768px wide to see that all of the form elements are inline, left
                    aligned, and the labels are alongside.</p>
                <form className="form-inline" action="/action_page.php">
                    <div className="form-group">
                        <label className="sr-only" htmlFor="email">Email:</label>
                        <input type="email" className="form-control" id="email" placeholder="Enter email" name="email"/>
                    </div>
                    <div className="form-group">
                        <label className="sr-only" htmlFor="pwd">Password:</label>
                        <input type="password" className="form-control" id="pwd" placeholder="Enter password"
                               name="pwd"/>
                    </div>
                    <div className="checkbox">
                        <label><input type="checkbox" name="remember"/> Remember me</label>
                    </div>
                    <button type="submit" className="btn btn-default">Submit</button>
                </form>
            </div>

            <hr/>

            <div className="container">
                <h2>Horizontal form</h2>
                <form className="form-horizontal" action="/action_page.php">
                    <div className="form-group">
                        <label className="control-label col-sm-2" htmlFor="email">Email:</label>
                        <div className="col-sm-10">
                            <input type="email" className="form-control" id="email" placeholder="Enter email"
                                   name="email"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="control-label col-sm-2" htmlFor="pwd">Password:</label>
                        <div className="col-sm-10">
                            <input type="password" className="form-control" id="pwd" placeholder="Enter password"
                                   name="pwd"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-10">
                            <div className="checkbox">
                                <label><input type="checkbox" name="remember"/> Remember me</label>
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-10">
                            <button type="submit" className="btn btn-default">Submit</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>;
    }
    public onReady(): void {
    }
}