
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget026 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_carousel.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Carousel Example</h2>
                <div id="myCarousel" className="carousel slide" data-ride="carousel">

                    <ol className="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" className="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>

                    <div className="carousel-inner">
                        <div className="item active">
                            <img src="https://www.w3schools.com/bootstrap/la.jpg" alt="Los Angeles" data-style="width:100%;"/>
                        </div>

                        <div className="item">
                            <img src="https://www.w3schools.com/bootstrap/chicago.jpg" alt="Chicago" data-style="width:100%;"/>
                        </div>

                        <div className="item">
                            <img src="https://www.w3schools.com/bootstrap/ny.jpg" alt="New york" data-style="width:100%;"/>
                        </div>
                    </div>

                    <a className="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span className="glyphicon glyphicon-chevron-left"></span>
                        <span className="sr-only">Previous</span>
                    </a>
                    <a className="right carousel-control" href="#myCarousel" data-slide="next">
                        <span className="glyphicon glyphicon-chevron-right"></span>
                        <span className="sr-only">Next</span>
                    </a>
                </div>
            </div>

            <hr/>

            <div className="container">
                <h2>Carousel Example</h2>
                <div id="myCarousel" className="carousel slide" data-ride="carousel">

                    <ol className="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" className="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>

                    <div className="carousel-inner">

                        <div className="item active">
                            <img src="https://www.w3schools.com/bootstrap/la.jpg" alt="Los Angeles" data-style="width:100%;"/>
                                <div className="carousel-caption">
                                    <h3>Los Angeles</h3>
                                    <p>LA is always so much fun!</p>
                                </div>
                        </div>

                        <div className="item">
                            <img src="https://www.w3schools.com/bootstrap/chicago.jpg" alt="Chicago" data-style="width:100%;"/>
                                <div className="carousel-caption">
                                    <h3>Chicago</h3>
                                    <p>Thank you, Chicago!</p>
                                </div>
                        </div>

                        <div className="item">
                            <img src="https://www.w3schools.com/bootstrap/ny.jpg" alt="New York" data-style="width:100%;"/>
                                <div className="carousel-caption">
                                    <h3>New York</h3>
                                    <p>We love the Big Apple!</p>
                                </div>
                        </div>

                    </div>

                    <a className="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span className="glyphicon glyphicon-chevron-left"></span>
                        <span className="sr-only">Previous</span>
                    </a>
                    <a className="right carousel-control" href="#myCarousel" data-slide="next">
                        <span className="glyphicon glyphicon-chevron-right"></span>
                        <span className="sr-only">Next</span>
                    </a>
                </div>
            </div>

        </div>;
    }
    public onReady(): void {
    }
}