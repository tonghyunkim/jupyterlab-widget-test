
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget015 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_list_groups.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Basic List Group</h2>
                <ul className="list-group">
                    <li className="list-group-item">First item</li>
                    <li className="list-group-item">Second item</li>
                    <li className="list-group-item">Third item</li>
                </ul>
            </div>

            <div className="container">
                <h2>List Group With Badges</h2>
                <ul className="list-group">
                    <li className="list-group-item">New <span className="badge">12</span></li>
                    <li className="list-group-item">Deleted <span className="badge">5</span></li>
                    <li className="list-group-item">Warnings <span className="badge">3</span></li>
                </ul>
            </div>

            <div className="container">
                <h2>List Group With Linked Items</h2>
                <div className="list-group">
                    <a href="#" className="list-group-item">First item</a>
                    <a href="#" className="list-group-item">Second item</a>
                    <a href="#" className="list-group-item">Third item</a>
                </div>
            </div>

            <div className="container">
                <h2>Active Item in a List Group</h2>
                <div className="list-group">
                    <a href="#" className="list-group-item active">First item</a>
                    <a href="#" className="list-group-item">Second item</a>
                    <a href="#" className="list-group-item">Third item</a>
                </div>
            </div>

            <div className="container">
                <h2>List Group With a Disabled Item</h2>
                <div className="list-group">
                    <a href="#" className="list-group-item disabled">First item</a>
                    <a href="#" className="list-group-item">Second item</a>
                    <a href="#" className="list-group-item">Third item</a>
                </div>
            </div>

            <div className="container">
                <h2>List Group With Contextual Classes</h2>
                <ul className="list-group">
                    <li className="list-group-item list-group-item-success">First item</li>
                    <li className="list-group-item list-group-item-info">Second item</li>
                    <li className="list-group-item list-group-item-warning">Third item</li>
                    <li className="list-group-item list-group-item-danger">Fourth item</li>
                </ul>

                <h2>Linked Items With Contextual Classes</h2>
                <p>Move the mouse over the linked items to see the hover effect:</p>
                <div className="list-group">
                    <a href="#" className="list-group-item list-group-item-success">First item</a>
                    <a href="#" className="list-group-item list-group-item-info">Second item</a>
                    <a href="#" className="list-group-item list-group-item-warning">Third item</a>
                    <a href="#" className="list-group-item list-group-item-danger">Fourth item</a>
                </div>
            </div>

            <div className="container">
                <h2>List Group With Custom Content</h2>
                <div className="list-group">
                    <a href="#" className="list-group-item active">
                        <h4 className="list-group-item-heading">First List Group Item Heading</h4>
                        <p className="list-group-item-text">List Group Item Text</p>
                    </a>
                    <a href="#" className="list-group-item">
                        <h4 className="list-group-item-heading">Second List Group Item Heading</h4>
                        <p className="list-group-item-text">List Group Item Text</p>
                    </a>
                    <a href="#" className="list-group-item">
                        <h4 className="list-group-item-heading">Third List Group Item Heading</h4>
                        <p className="list-group-item-text">List Group Item Text</p>
                    </a>
                </div>
            </div>
            
        </div>;
    }
    public onReady(): void {
    }
}