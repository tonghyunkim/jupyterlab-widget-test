import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget032 extends AbstractVDomRenderer {
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div data-spy="scroll" data-target=".navbar" data-offset="50">

                <div className="container">
                    <div className="well">this widget is an example of Bootstrap Tutorial <a
                        href="https://www.w3schools.com/bootstrap/bootstrap_filters.asp" target="_blank">here</a>
                    </div>
                </div>

                <div className="container-fluid" data-style="background-color:#F44336;color:#fff;height:200px;">
                    <h1>Bootstrap Affix Example</h1>
                    <h3>Fixed (sticky) navbar on scroll</h3>
                    <p>Scroll this page to see how the navbar behaves with data-spy="affix".</p>
                    <p>The navbar is attached to the top of the page after you have scrolled a specified amount of
                        pixels.</p>
                </div>

                <nav className="navbar navbar-inverse" data-spy="affix" data-offset-top="197">
                    <ul className="nav navbar-nav">
                        <li className="active"><a href="#">Basic Topnav</a></li>
                        <li><a href="#">Page 1</a></li>
                        <li><a href="#">Page 2</a></li>
                        <li><a href="#">Page 3</a></li>
                    </ul>
                </nav>

                <div className="container-fluid" data-style="height:1000px">
                    <h1>Some text to enable scrolling</h1>
                    <h1>Some text to enable scrolling</h1>
                    <h1>Some text to enable scrolling</h1>
                    <h1>Some text to enable scrolling</h1>
                    <h1>Some text to enable scrolling</h1>
                    <h1>Some text to enable scrolling</h1>
                    <h1>Some text to enable scrolling</h1>
                    <h1>Some text to enable scrolling</h1>
                    <h1>Some text to enable scrolling</h1>
                    <h1>Some text to enable scrolling</h1>
                    <h1>Some text to enable scrolling</h1>
                </div>

            </div>

        </div>;
    }

    public onReady(): void {

    }
}