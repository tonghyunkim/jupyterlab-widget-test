
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget012 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {

        /** @type {{search: React.CSSProperties}} */
        const styles = {
            search: {
                position: "relative",
            }
        };

        /*

        https://stackoverflow.com/questions/46215614/property-does-not-exist-on-type-detailedhtmlprops-htmldivelement-with-react

         */


        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_progressbars.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Basic Progress Bar</h2>
                <div className="progress">
                    <div className="progress-bar" data-role="progressbar" data-aria-valuenow="70" data-aria-valuemin="0"
                         data-aria-valuemax="100" data-style="width:70%">
                        <span className="sr-only">70% Complete</span>
                    </div>
                </div>
            </div>

            <div className="container">
                <h2>Progress Bar With Label</h2>
                <div className="progress">
                    <div className="progress-bar" data-role="progressbar" data-aria-valuenow="70" data-aria-valuemin="0"
                         data-aria-valuemax="100" data-style="width:70%">
                        70%
                    </div>
                </div>
            </div>

            <div className="container">
                <h2>Colored Progress Bars</h2>
                <p>The contextual classes colors the progress bars:</p>
                <div className="progress">
                    <div className="progress-bar progress-bar-success" data-role="progressbar" data-aria-valuenow="40"
                         data-aria-valuemin="0" data-aria-valuemax="100" data-style="width:40%">
                        40% Complete (success)
                    </div>
                </div>
                <div className="progress">
                    <div className="progress-bar progress-bar-info" data-role="progressbar" data-aria-valuenow="50"
                         data-aria-valuemin="0" data-aria-valuemax="100" data-style="width:50%">
                        50% Complete (info)
                    </div>
                </div>
                <div className="progress">
                    <div className="progress-bar progress-bar-warning" data-role="progressbar" data-aria-valuenow="60"
                         data-aria-valuemin="0" data-aria-valuemax="100" data-style="width:60%">
                        60% Complete (warning)
                    </div>
                </div>
                <div className="progress">
                    <div className="progress-bar progress-bar-danger" data-role="progressbar" data-aria-valuenow="70"
                         data-aria-valuemin="0" data-aria-valuemax="100" data-style="width:70%">
                        70% Complete (danger)
                    </div>
                </div>
            </div>

            <div className="container">
                <h2>Striped Progress Bars</h2>
                <p>The .progress-bar-striped class adds stripes to the progress bars:</p>
                <div className="progress">
                    <div className="progress-bar progress-bar-success progress-bar-striped" data-role="progressbar"
                         data-aria-valuenow="40" data-aria-valuemin="0" data-aria-valuemax="100" data-style="width:40%">
                        40% Complete (success)
                    </div>
                </div>
                <div className="progress">
                    <div className="progress-bar progress-bar-info progress-bar-striped" data-role="progressbar"
                         data-aria-valuenow="50" data-aria-valuemin="0" data-aria-valuemax="100" data-style="width:50%">
                        50% Complete (info)
                    </div>
                </div>
                <div className="progress">
                    <div className="progress-bar progress-bar-warning progress-bar-striped" data-role="progressbar"
                         data-aria-valuenow="60" data-aria-valuemin="0" data-aria-valuemax="100" data-style="width:60%">
                        60% Complete (warning)
                    </div>
                </div>
                <div className="progress">
                    <div className="progress-bar progress-bar-danger progress-bar-striped" data-role="progressbar"
                         data-aria-valuenow="70" data-aria-valuemin="0" data-aria-valuemax="100" data-style="width:70%">
                        70% Complete (danger)
                    </div>
                </div>
            </div>

            <div className="container">
                <h2>Animated Progress Bar</h2>
                <p>The .active class animates the progress bar:</p>
                <div className="progress">
                    <div className="progress-bar progress-bar-striped active" data-role="progressbar" data-aria-valuenow="40"
                         data-aria-valuemin="0" data-aria-valuemax="100" data-style="width:40%">
                        40%
                    </div>
                </div>
            </div>

            <div className="container">
                <h2>Stacked Progress Bars</h2>
                <p>Create a stacked progress bar by placing multiple bars into the same div with class .progress:</p>
                <div className="progress">
                    <div className="progress-bar progress-bar-success" data-role="progressbar" data-style="width:40%">
                        Free Space
                    </div>
                    <div className="progress-bar progress-bar-warning" data-role="progressbar" data-style="width:10%">
                        Warning
                    </div>
                    <div className="progress-bar progress-bar-danger" data-role="progressbar" data-style="width:20%">
                        Danger
                    </div>
                </div>
            </div>
            
        </div>;
    }
    public onReady(): void {
    }
}