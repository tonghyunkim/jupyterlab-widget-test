
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget029 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_popover.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h3>Popover Example</h3>
                <a href="#" data-toggle="popover" title="Popover Header" data-content="Some content inside the popover">Toggle
                    popover</a>
            </div>

            <hr/>

            <div className="container">
                <h3>Popover Example</h3>
                <ul className="list-inline">
                    <li><a href="#" title="Header" data-toggle="popover" data-placement="top"
                           data-content="Content">Top</a></li>
                    <li><a href="#" title="Header" data-toggle="popover" data-placement="bottom"
                           data-content="Content">Bottom</a></li>
                    <li><a href="#" title="Header" data-toggle="popover" data-placement="left"
                           data-content="Content">Left</a></li>
                    <li><a href="#" title="Header" data-toggle="popover" data-placement="right"
                           data-content="Content">Right</a></li>
                </ul>
            </div>

            <hr/>

            <div className="container">
                <h3>Popover Example</h3>
                <a href="#" title="Dismissible popover" data-toggle="popover" data-trigger="focus"
                   data-content="Click anywhere in the document to close this popover">Click me</a>
            </div>

            <hr/>

            <div className="container">
                <h3>Popover Example</h3>
                <a href="#" title="Header" data-toggle="popover" data-content="Some content">Click Me</a><br/>
                <a href="#" title="Header" data-toggle="popover" data-trigger="hover" data-content="Some content">Hover
                    over me</a>
            </div>


        </div>;
    }
    public onReady(): void {

        ($('[data-toggle="popover"]') as any).popover();

    }
}