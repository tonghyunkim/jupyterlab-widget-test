
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget016 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_panels.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Basic Panel</h2>
                <div className="panel panel-default">
                    <div className="panel-body">A Basic Panel</div>
                </div>
            </div>

            <div className="container">
                <h2>Panel Heading</h2>
                <div className="panel panel-default">
                    <div className="panel-heading">Panel Heading</div>
                    <div className="panel-body">Panel Content</div>
                </div>
            </div>

            <div className="container">
                <h2>Panel Footer</h2>
                <div className="panel panel-default">
                    <div className="panel-heading">Panel Heading</div>
                    <div className="panel-body">Panel Content</div>
                    <div className="panel-footer">Panel Footer</div>
                </div>
            </div>

            <div className="container">
                <h2>Panel Group</h2>
                <p>The panel-group class clears the bottom-margin. Try to remove the class and see what happens.</p>
                <div className="panel-group">
                    <div className="panel panel-default">
                        <div className="panel-heading">Panel Header</div>
                        <div className="panel-body">Panel Content</div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">Panel Header</div>
                        <div className="panel-body">Panel Content</div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">Panel Header</div>
                        <div className="panel-body">Panel Content</div>
                    </div>
                </div>
            </div>

            <div className="container">
                <h2>Panels with Contextual Classes</h2>
                <div className="panel-group">
                    <div className="panel panel-default">
                        <div className="panel-heading">Panel with panel-default class</div>
                        <div className="panel-body">Panel Content</div>
                    </div>

                    <div className="panel panel-primary">
                        <div className="panel-heading">Panel with panel-primary class</div>
                        <div className="panel-body">Panel Content</div>
                    </div>

                    <div className="panel panel-success">
                        <div className="panel-heading">Panel with panel-success class</div>
                        <div className="panel-body">Panel Content</div>
                    </div>

                    <div className="panel panel-info">
                        <div className="panel-heading">Panel with panel-info class</div>
                        <div className="panel-body">Panel Content</div>
                    </div>

                    <div className="panel panel-warning">
                        <div className="panel-heading">Panel with panel-warning class</div>
                        <div className="panel-body">Panel Content</div>
                    </div>

                    <div className="panel panel-danger">
                        <div className="panel-heading">Panel with panel-danger class</div>
                        <div className="panel-body">Panel Content</div>
                    </div>
                </div>
            </div>

        </div>;
    }
    public onReady(): void {
    }
}