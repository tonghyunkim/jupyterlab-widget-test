
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget008 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_buttons.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Button Styles</h2>
                <button type="button" className="btn">Basic</button>
                <button type="button" className="btn btn-default">Default</button>
                <button type="button" className="btn btn-primary">Primary</button>
                <button type="button" className="btn btn-success">Success</button>
                <button type="button" className="btn btn-info">Info</button>
                <button type="button" className="btn btn-warning">Warning</button>
                <button type="button" className="btn btn-danger">Danger</button>
                <button type="button" className="btn btn-link">Link</button>
            </div>

            <div className="container">
                <h2>Button Tags</h2>
                <a href="#" className="btn btn-info" role="button">Link Button</a>
                <button type="button" className="btn btn-info">Button</button>
                <input type="button" className="btn btn-info" value="Input Button" />
                <input type="submit" className="btn btn-info" value="Submit Button" />
            </div>

            <div className="container">
                <h2>Button Sizes</h2>
                <button type="button" className="btn btn-primary btn-lg">Large</button>
                <button type="button" className="btn btn-primary btn-md">Medium</button>
                <button type="button" className="btn btn-primary btn-sm">Small</button>
                <button type="button" className="btn btn-primary btn-xs">XSmall</button>
            </div>

            <div className="container">
                <h2>Block Level Buttons</h2>
                <button type="button" className="btn btn-primary btn-block">Button 1</button>
                <button type="button" className="btn btn-default btn-block">Button 2</button>

                <h2>Large Block Level Buttons</h2>
                <button type="button" className="btn btn-primary btn-lg btn-block">Button 1</button>
                <button type="button" className="btn btn-default btn-lg btn-block">Button 2</button>

                <h2>Small Block Level Buttons</h2>
                <button type="button" className="btn btn-primary btn-sm btn-block">Button 1</button>
                <button type="button" className="btn btn-default btn-sm btn-block">Button 2</button>
            </div>

            <div className="container">
                <h2>Button States</h2>
                <button type="button" className="btn btn-primary">Primary Button</button>
                <button type="button" className="btn btn-primary active">Active Primary</button>
                <button type="button" className="btn btn-primary disabled">Disabled Primary</button>
            </div>

        </div>;
    }
    public onReady(): void {
    }
}