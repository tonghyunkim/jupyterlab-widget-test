import * as React from "react";
import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";
import {checkloadjscssfile} from "../index";

/**
 * to build correctly, install these.
 * jlpm add bootstrap
 * jlpm add jquery
 * jlpm add @types/jquery
 * jlpm add popper.js
 */

/**
 * inspired by https://jsfiddle.net/IrvinDominin/xXaxh/
 *
 * warning "jupyterlab_widget_test > bootstrap@4.1.1" has unmet peer dependency "jquery@1.9.1 - 3".
 * warning "jupyterlab_widget_test > bootstrap@4.1.1" has unmet peer dependency "popper.js@^1.14.3".
 */
export class Widget120 extends AbstractVDomRenderer {
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>
            <div id="myCarousel" className="carousel slide">
                <div className="carousel-inner">
                    <div className="active item">
                        <img src="http://placehold.it/300x200/888&text=Item 1"/>
                    </div>
                    <div className="item">
                        <img src="http://placehold.it/300x200/aaa&text=Item 2"/>
                    </div>
                    <div className="item">
                        <img src="http://placehold.it/300x200/444&text=Item 3"/>
                    </div>
                </div>
                <a className="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                <a className="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
            </div>
            <ol className="carousel-linked-nav pagination">
                <li className="active"><a href="#1">1</a>
                </li>
                <li><a href="#2">2</a>
                </li>
                <li><a href="#3">3</a>
                </li>
            </ol>
        </div>;
    }

    public onReady(): void {

        /* this uses bootstrap 3.3.7 */
        // checkloadjscssfile("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js", "js");
        // checkloadjscssfile("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css", "css");
        // checkloadjscssfile("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css", "css");

        // invoke the carousel
        ($('#myCarousel') as any).carousel({interval: 1000});
        ($('#myCarousel') as any).carousel('pause');

        /* SLIDE ON CLICK */

        $('.carousel-linked-nav > li > a').click(function () {

            // grab href, remove pound sign, convert to number
            var item = Number($(this).attr('href').substring(1));

            // slide to number -1 (account for zero indexing)
            ($('#myCarousel') as any).carousel(item - 1);

            // remove current active class
            $('.carousel-linked-nav .active').removeClass('active');

            // add active class to just clicked on item
            $(this).parent().addClass('active');

            // don't follow the link
            return false;
        });

        /* AUTOPLAY NAV HIGHLIGHT */

        // bind 'slid' function
        ($('#myCarousel') as any).bind('slid', function () {

            // remove active class
            $('.carousel-linked-nav .active').removeClass('active');

            // get index of currently active item
            var idx = $('#myCarousel .item.active').index();

            // select currently active item and add active class
            $('.carousel-linked-nav li:eq(' + idx + ')').addClass('active');

        });
    }
}