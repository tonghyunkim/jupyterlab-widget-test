
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget007 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_alerts.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Alerts</h2>
                <div className="alert alert-success">
                    <strong>Success!</strong> This alert box could indicate a successful or positive action.
                </div>
                <div className="alert alert-info">
                    <strong>Info!</strong> This alert box could indicate a neutral informative change or action.
                </div>
                <div className="alert alert-warning">
                    <strong>Warning!</strong> This alert box could indicate a warning that might need attention.
                </div>
                <div className="alert alert-danger">
                    <strong>Danger!</strong> This alert box could indicate a dangerous or potentially negative action.
                </div>
            </div>

            <div className="container">
                <h2>Alert Links</h2>
                <p>Add the alert-link class to any links inside the alert box to create "matching colored links".</p>
                <div className="alert alert-success">
                    <strong>Success!</strong> You should <a href="#" className="alert-link">read this message</a>.
                </div>
                <div className="alert alert-info">
                    <strong>Info!</strong> You should <a href="#" className="alert-link">read this message</a>.
                </div>
                <div className="alert alert-warning">
                    <strong>Warning!</strong> You should <a href="#" className="alert-link">read this message</a>.
                </div>
                <div className="alert alert-danger">
                    <strong>Danger!</strong> You should <a href="#" className="alert-link">read this message</a>.
                </div>
            </div>

            <div className="container">
                <h2>Alerts</h2>
                <p>The a element with class="close" and data-dismiss="alert" is used to close the alert box.</p>
                <p>The alert-dismissible class adds some extra padding to the close button.</p>
                <div className="alert alert-success alert-dismissible">
                    <a href="#" className="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> This alert box could indicate a successful or positive action.
                </div>
                <div className="alert alert-info alert-dismissible">
                    <a href="#" className="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Info!</strong> This alert box could indicate a neutral informative change or action.
                </div>
                <div className="alert alert-warning alert-dismissible">
                    <a href="#" className="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Warning!</strong> This alert box could indicate a warning that might need attention.
                </div>
                <div className="alert alert-danger alert-dismissible">
                    <a href="#" className="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Danger!</strong> This alert box could indicate a dangerous or potentially negative action.
                </div>
            </div>

            <div className="container">
                <h2>Animated Alerts</h2>
                <p>The .fade and .in classes adds a fading effect when closing the alert message.</p>
                <div className="alert alert-success alert-dismissible fade in">
                    <a href="#" className="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> This alert box could indicate a successful or positive action.
                </div>
                <div className="alert alert-info alert-dismissible fade in">
                    <a href="#" className="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Info!</strong> This alert box could indicate a neutral informative change or action.
                </div>
                <div className="alert alert-warning alert-dismissible fade in">
                    <a href="#" className="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Warning!</strong> This alert box could indicate a warning that might need attention.
                </div>
                <div className="alert alert-danger alert-dismissible fade in">
                    <a href="#" className="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Danger!</strong> This alert box could indicate a dangerous or potentially negative action.
                </div>
            </div>

        </div>;
    }
    public onReady(): void {
    }
}