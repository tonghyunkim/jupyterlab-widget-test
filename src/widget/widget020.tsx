
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget020 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_navbar.asp" target="_blank">here</a></div>
            </div>

            <nav className="navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="#">WebSiteName</a>
                    </div>
                    <ul className="nav navbar-nav">
                        <li className="active"><a href="#">Home</a></li>
                        <li><a href="#">Page 1</a></li>
                        <li><a href="#">Page 2</a></li>
                        <li><a href="#">Page 3</a></li>
                    </ul>
                </div>
            </nav>

            <div className="container">
                <h3>Basic Navbar Example</h3>
                <p>A navigation bar is a navigation header that is placed at the top of the page.</p>
            </div>

            <hr/>

            <nav className="navbar navbar-inverse">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="#">WebSiteName</a>
                    </div>
                    <ul className="nav navbar-nav">
                        <li className="active"><a href="#">Home</a></li>
                        <li><a href="#">Page 1</a></li>
                        <li><a href="#">Page 2</a></li>
                        <li><a href="#">Page 3</a></li>
                    </ul>
                </div>
            </nav>

            <div className="container">
                <h3>Inverted Navbar</h3>
                <p>An inverted navbar is black instead of gray.</p>
            </div>

            <hr/>

            <nav className="navbar navbar-inverse">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="#">WebSiteName</a>
                    </div>
                    <ul className="nav navbar-nav">
                        <li className="active"><a href="#">Home</a></li>
                        <li className="dropdown"><a className="dropdown-toggle" data-toggle="dropdown" href="#">Page
                            1 <span className="caret"></span></a>
                            <ul className="dropdown-menu">
                                <li><a href="#">Page 1-1</a></li>
                                <li><a href="#">Page 1-2</a></li>
                                <li><a href="#">Page 1-3</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Page 2</a></li>
                        <li><a href="#">Page 3</a></li>
                    </ul>
                </div>
            </nav>

            <div className="container">
                <h3>Navbar With Dropdown</h3>
                <p>This example adds a dropdown menu for the "Page 1" button in the navigation bar.</p>
            </div>

            <hr/>

            <nav className="navbar navbar-inverse">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="#">WebSiteName</a>
                    </div>
                    <ul className="nav navbar-nav">
                        <li className="active"><a href="#">Home</a></li>
                        <li className="dropdown"><a className="dropdown-toggle" data-toggle="dropdown" href="#">Page
                            1 <span className="caret"></span></a>
                            <ul className="dropdown-menu">
                                <li><a href="#">Page 1-1</a></li>
                                <li><a href="#">Page 1-2</a></li>
                                <li><a href="#">Page 1-3</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Page 2</a></li>
                    </ul>
                    <ul className="nav navbar-nav navbar-right">
                        <li><a href="#"><span className="glyphicon glyphicon-user"></span> Sign Up</a></li>
                        <li><a href="#"><span className="glyphicon glyphicon-log-in"></span> Login</a></li>
                    </ul>
                </div>
            </nav>

            <div className="container">
                <h3>Right Aligned Navbar</h3>
                <p>The .navbar-right class is used to right-align navigation bar buttons.</p>
            </div>

            <hr/>

            <nav className="navbar navbar-inverse">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="#">WebSiteName</a>
                    </div>
                    <ul className="nav navbar-nav">
                        <li className="active"><a href="#">Home</a></li>
                        <li><a href="#">Page 1</a></li>
                        <li><a href="#">Page 2</a></li>
                    </ul>
                    <form className="navbar-form navbar-left" action="/action_page.php">
                        <div className="form-group">
                            <input type="text" className="form-control" placeholder="Search" name="search"/>
                        </div>
                        <button type="submit" className="btn btn-default">Submit</button>
                    </form>
                </div>
            </nav>

            <div className="container">
                <h3>Navbar Forms</h3>
                <p>Use the .navbar-form class to vertically align form elements (same padding as links) inside the
                    navbar.</p>
            </div>

            <hr/>

            <nav className="navbar navbar-inverse">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="#">WebSiteName</a>
                    </div>
                    <ul className="nav navbar-nav">
                        <li className="active"><a href="#">Home</a></li>
                        <li><a href="#">Page 1</a></li>
                        <li><a href="#">Page 2</a></li>
                    </ul>
                    <form className="navbar-form navbar-left" action="/action_page.php">
                        <div className="input-group">
                            <input type="text" className="form-control" placeholder="Search" name="search"/>
                                <div className="input-group-btn">
                                    <button className="btn btn-default" type="submit">
                                        <i className="glyphicon glyphicon-search"></i>
                                    </button>
                                </div>
                        </div>
                    </form>
                </div>
            </nav>

            <div className="container">
                <h3>Navbar Forms</h3>
                <p>Use the .navbar-form class to vertically align form elements (same padding as links) inside the
                    navbar.</p>
                <p>The .input-group class is a container to enhance an input by adding an icon, text or a button in
                    front or behind it as a "help text".</p>
                <p>The .input-group-btn class attaches a button next to an input field. This is often used as a search
                    bar:</p>
            </div>

            <hr/>

            <nav className="navbar navbar-inverse">
                <ul className="nav navbar-nav">
                    <li><a href="#">Link</a></li>
                    <li><a href="#">Link</a></li>
                </ul>
                <p className="navbar-text">Some text</p>
            </nav>

            <div className="container">
                <h3>Navbar Text</h3>
                <p>Use the .navbar-text class to vertical align any elements inside the navbar that are not links
                    (ensures proper padding).</p>
            </div>

            <hr/>

            <nav className="navbar navbar-inverse navbar-fixed-top">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="#">WebSiteName</a>
                    </div>
                    <ul className="nav navbar-nav">
                        <li className="active"><a href="#">Home</a></li>
                        <li><a href="#">Page 1</a></li>
                        <li><a href="#">Page 2</a></li>
                        <li><a href="#">Page 3</a></li>
                    </ul>
                </div>
            </nav>

            <div className="container" data-style="margin-top:50px">
                <h3>Fixed Navbar</h3>
                <div className="row">
                    <div className="col-md-4">
                        <p>A fixed navigation bar stays visible in a fixed position (top or bottom) independent of the
                            page scroll.</p>
                        <p>A fixed navigation bar stays visible in a fixed position (top or bottom) independent of the
                            page scroll.</p>
                    </div>
                    <div className="col-md-4">
                        <p>A fixed navigation bar stays visible in a fixed position (top or bottom) independent of the
                            page scroll.</p>
                        <p>A fixed navigation bar stays visible in a fixed position (top or bottom) independent of the
                            page scroll.</p>
                    </div>
                    <div className="col-md-4">
                        <p>A fixed navigation bar stays visible in a fixed position (top or bottom) independent of the
                            page scroll.</p>
                        <p>A fixed navigation bar stays visible in a fixed position (top or bottom) independent of the
                            page scroll.</p>
                    </div>
                </div>
            </div>

            <h1>Scroll this page to see the effect</h1>

            <hr/>

            <nav className="navbar navbar-inverse navbar-fixed-bottom">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="#">WebSiteName</a>
                    </div>
                    <ul className="nav navbar-nav">
                        <li className="active"><a href="#">Home</a></li>
                        <li><a href="#">Page 1</a></li>
                        <li><a href="#">Page 2</a></li>
                        <li><a href="#">Page 3</a></li>
                    </ul>
                </div>
            </nav>

            <div className="container">
                <h3>Fixed Bottom Navbar</h3>
                <div className="row">
                    <div className="col-md-3">
                        <p>The .navbar-fixed-bottom class makes the navigation bar stay at the bottom.</p>
                    </div>
                    <div className="col-md-3">
                        <p>The .navbar-fixed-bottom class makes the navigation bar stay at the bottom.</p>
                    </div>
                    <div className="col-md-3">
                        <p>The .navbar-fixed-bottom class makes the navigation bar stay at the bottom.</p>
                    </div>
                    <div className="col-md-3">
                        <p>The .navbar-fixed-bottom class makes the navigation bar stay at the bottom.</p>
                    </div>
                    <div className="clearfix visible-lg"></div>
                </div>
            </div>

            <div className="container">
                <div className="row">
                    <div className="col-md-3">
                        <p>The .navbar-fixed-bottom class makes the navigation bar stay at the bottom.</p>
                    </div>
                    <div className="col-md-3">
                        <p>The .navbar-fixed-bottom class makes the navigation bar stay at the bottom.</p>
                    </div>
                    <div className="col-md-3">
                        <p>The .navbar-fixed-bottom class makes the navigation bar stay at the bottom.</p>
                    </div>
                    <div className="col-md-3">
                        <p>The .navbar-fixed-bottom class makes the navigation bar stay at the bottom.</p>
                    </div>
                    <div className="clearfix visible-lg"></div>
                </div>
            </div>

            <div className="container">
                <div className="row">
                    <div className="col-md-3">
                        <p>The .navbar-fixed-bottom class makes the navigation bar stay at the bottom.</p>
                    </div>
                    <div className="col-md-3">
                        <p>The .navbar-fixed-bottom class makes the navigation bar stay at the bottom.</p>
                    </div>
                    <div className="col-md-3">
                        <p>The .navbar-fixed-bottom class makes the navigation bar stay at the bottom.</p>
                    </div>
                    <div className="col-md-3">
                        <p>The .navbar-fixed-bottom class makes the navigation bar stay at the bottom.</p>
                    </div>
                    <div className="clearfix visible-lg"></div>
                </div>
            </div>

            <div className="container">
                <div className="row">
                    <div className="col-md-3">
                        <p>The .navbar-fixed-bottom class makes the navigation bar stay at the bottom.</p>
                    </div>
                    <div className="col-md-3">
                        <p>The .navbar-fixed-bottom class makes the navigation bar stay at the bottom.</p>
                    </div>
                    <div className="col-md-3">
                        <p>The .navbar-fixed-bottom class makes the navigation bar stay at the bottom.</p>
                    </div>
                    <div className="col-md-3">
                        <p>The .navbar-fixed-bottom class makes the navigation bar stay at the bottom.</p>
                    </div>
                    <div className="clearfix visible-lg"></div>
                </div>
            </div>

            <hr/>

            <nav className="navbar navbar-inverse">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand" href="#">WebSiteName</a>
                    </div>
                    <div className="collapse navbar-collapse" id="myNavbar">
                        <ul className="nav navbar-nav">
                            <li className="active"><a href="#">Home</a></li>
                            <li className="dropdown">
                                <a className="dropdown-toggle" data-toggle="dropdown" href="#">Page 1 <span
                                    className="caret"></span></a>
                                <ul className="dropdown-menu">
                                    <li><a href="#">Page 1-1</a></li>
                                    <li><a href="#">Page 1-2</a></li>
                                    <li><a href="#">Page 1-3</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Page 2</a></li>
                            <li><a href="#">Page 3</a></li>
                        </ul>
                        <ul className="nav navbar-nav navbar-right">
                            <li><a href="#"><span className="glyphicon glyphicon-user"></span> Sign Up</a></li>
                            <li><a href="#"><span className="glyphicon glyphicon-log-in"></span> Login</a></li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div className="container">
                <h3>Collapsible Navbar</h3>
                <p>In this example, the navigation bar is hidden on small screens and replaced by a button in the top
                    right corner (try to re-size this window).</p>
                    <p>Only when the button is clicked, the navigation bar will be displayed.</p>
            </div>

        </div>;
    }
    public onReady(): void {
    }
}