
import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget009 extends AbstractVDomRenderer{
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {
        return <div>

            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_button_groups.asp" target="_blank">here</a></div>
            </div>

            <div className="container">
                <h2>Button Group</h2>
                <p>The .btn-group class creates a button group:</p>
                <div className="btn-group">
                    <button type="button" className="btn btn-primary">Apple</button>
                    <button type="button" className="btn btn-primary">Samsung</button>
                    <button type="button" className="btn btn-primary">Sony</button>
                </div>
            </div>

            <div className="container">
                <h2>Button Groups - Set Sizes</h2>
                <p>Add class .btn-group-* to size all buttons in a button group.</p>
                <h3>Large Buttons:</h3>
                <div className="btn-group btn-group-lg">
                    <button type="button" className="btn btn-primary">Apple</button>
                    <button type="button" className="btn btn-primary">Samsung</button>
                    <button type="button" className="btn btn-primary">Sony</button>
                </div>
                <h3>Default Buttons:</h3>
                <div className="btn-group">
                    <button type="button" className="btn btn-primary">Apple</button>
                    <button type="button" className="btn btn-primary">Samsung</button>
                    <button type="button" className="btn btn-primary">Sony</button>
                </div>
                <h3>Small Buttons:</h3>
                <div className="btn-group btn-group-sm">
                    <button type="button" className="btn btn-primary">Apple</button>
                    <button type="button" className="btn btn-primary">Samsung</button>
                    <button type="button" className="btn btn-primary">Sony</button>
                </div>
                <h3>Extra Small Buttons:</h3>
                <div className="btn-group btn-group-xs">
                    <button type="button" className="btn btn-primary">Apple</button>
                    <button type="button" className="btn btn-primary">Samsung</button>
                    <button type="button" className="btn btn-primary">Sony</button>
                </div>
            </div>

            <div className="container">
                <h2>Vertical Button Group</h2>
                <p>Use the .btn-group-vertical class to create a vertical button group:</p>
                <div className="btn-group-vertical">
                    <button type="button" className="btn btn-primary">Apple</button>
                    <button type="button" className="btn btn-primary">Samsung</button>
                    <button type="button" className="btn btn-primary">Sony</button>
                </div>
            </div>

            <div className="container">
                <h2>Justified Button Groups</h2>
                <p>To span the entire width of the screen, use the .btn-group-justified class:</p>
                <div className="btn-group btn-group-justified">
                    <a href="#" className="btn btn-primary">Apple</a>
                    <a href="#" className="btn btn-primary">Samsung</a>
                    <a href="#" className="btn btn-primary">Sony</a>
                </div>
            </div>

            <div className="container">
                <h2>Justified Button Groups</h2>
                <div className="btn-group btn-group-justified">
                    <div className="btn-group">
                        <button type="button" className="btn btn-primary">Apple</button>
                    </div>
                    <div className="btn-group">
                        <button type="button" className="btn btn-primary">Samsung</button>
                    </div>
                    <div className="btn-group">
                        <button type="button" className="btn btn-primary">Sony</button>
                    </div>
                </div>
            </div>

            <div className="container">
                <h2>Nesting Button Groups</h2>
                <p>Nest button groups to create drop down menus:</p>
                <div className="btn-group">
                    <button type="button" className="btn btn-primary">Apple</button>
                    <button type="button" className="btn btn-primary">Samsung</button>
                    <div className="btn-group">
                        <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            Sony <span className="caret"></span></button>
                        <ul className="dropdown-menu" role="menu">
                            <li><a href="#">Tablet</a></li>
                            <li><a href="#">Smartphone</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div className="container">
                <h2>Split Buttons</h2>
                <div className="btn-group">
                    <button type="button" className="btn btn-primary">Sony</button>
                    <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        <span className="caret"></span>
                    </button>
                    <ul className="dropdown-menu" role="menu">
                        <li><a href="#">Tablet</a></li>
                        <li><a href="#">Smartphone</a></li>
                    </ul>
                </div>
            </div>
            
        </div>;
    }
    public onReady(): void {
    }
}