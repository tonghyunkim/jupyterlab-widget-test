import * as React from "react";

import {AbstractVDomRenderer} from "../core/abstractvdomrenderer";

/**
 * this widget uses bootstrap3
 */
export class Widget001 extends AbstractVDomRenderer {
    protected render(): Array<React.ReactElement<any>> | React.ReactElement<any> | null {

        /**
         * uses container or container-fluid
         * @see https://www.w3schools.com/bootstrap/bootstrap_get_started.asp
         */
        return <div>
            <div className="container">
                <div className="well">this widget is an example of Bootstrap Tutorial <a
                    href="https://www.w3schools.com/bootstrap/bootstrap_grid_basic.asp" target="_blank">here</a></div>
                <div className="row">
                    <div className="col-sm-4">.col-sm-4</div>
                    <div className="col-sm-4">.col-sm-4</div>
                    <div className="col-sm-4">.col-sm-4</div>
                </div>
            </div>
            <hr/>
            <div className="container">
                <div className="row">
                    <div className="col-sm-4">.col-sm-4</div>
                    <div className="col-sm-8">.col-sm-8</div>
                </div>
            </div>
        </div>;
    }

    public onReady(): void {

    }
}