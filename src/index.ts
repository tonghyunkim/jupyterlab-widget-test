import {
    JupyterLab, JupyterLabPlugin
} from '@jupyterlab/application';

import '../style/index.css';
import {activateWidgetsPalette} from "./palette/palette_widgets";


/**
 * Initialization data for the jupyterlab_widget_test extension.
 */
const extension: JupyterLabPlugin<void> = {
    id: 'jupyterlab_widget_test',
    autoStart: true,
    activate: (app: JupyterLab) => {
        console.log('JupyterLab extension jupyterlab_widget_test is activated!');
        activateWidgetsPalette(app);
    }
};

/**
 * Loads css or javascript file dynamically
 * @param filename
 * @param filetype
 * @author tkim
 */
function loadjscssfile(filename, filetype) {
    let fileref: any;
    if (filetype == "js") { //if filename is a external JavaScript file
        fileref = document.createElement('script');
        fileref.setAttribute("type", "text/javascript");
        fileref.setAttribute("src", filename);
        /*
        Modern browsers only execute scripts asyncrounously or deferred if async and defer has been specified.
        async=false and defer=false is completely unnecessary in HTML. However, scripts that are dynamically created
        have async set to true by default, which is why you need the scr.async = false;
        @see https://stackoverflow.com/questions/7308908/waiting-for-dynamically-loaded-script
         */
        fileref.async = false; // optionally.

    }
    else if (filetype == "css") { //if filename is an external CSS file
        fileref = document.createElement("link");
        fileref.setAttribute("rel", "stylesheet");
        fileref.setAttribute("type", "text/css");
        fileref.setAttribute("href", filename);
    }
    if (typeof fileref != "undefined")
        document.getElementsByTagName("head")[0].appendChild(fileref);
        /*
        let head = document.getElementsByTagName("head")[0];
        head.insertBefore(scr, head.firstChild);
        */
}

var filesadded = ""; //list of files already added

export function checkloadjscssfile(filename, filetype) {
    if (filesadded.indexOf("[" + filename + "]") == -1) {
        loadjscssfile(filename, filetype);
        console.log(filename + ' loaded.');
        filesadded += "[" + filename + "]" //List of files added in the form "[filename1],[filename2],etc"
    }
    else
        console.log(`${filename} file already added!`)
}

/**
 * loads jquery file dynamically in runtime
 * jquery should be loaded in order
 */
checkloadjscssfile("https://code.jquery.com/jquery-3.3.1.min.js", "js");

/**
 * loads popper.js bootstrap dynamically in runtime orderly
 * using bootstrap 3.3.7
 * bootstrap-theme is optional
 */
checkloadjscssfile("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css", "css");
checkloadjscssfile("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css", "css");
checkloadjscssfile("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js", "js");

/**
 * loads popper.js bootstrap dynamically in runtime orderly
 * using bootstrap 4.1.1
 * bootstrap-theme is not available in 4.1.1
 */
// checkloadjscssfile("https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css", "css");
// checkloadjscssfile("https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js", "js");


export default extension;
