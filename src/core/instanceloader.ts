/**
 * Creates instance dynamically
 * @see https://www.stevefenton.co.uk/2014/07/creating-typescript-classes-dynamically/
 * @author tkim
 * @example

 interface NamedThing {
        moreInput: string;
}

 class Example {
        public name = 'Janice';

        constructor(public input: string, public moreInput: string) {

        }
}

 var example = InstanceLoader.getInstance<NamedThing>(window, 'Example', 'Some input', 'Some more input');
 alert(example.moreInput);

 *
 */

import { Store } from "../widget/Store";

/**
 * inspired by https://gist.github.com/buddhics/a29ca8ac4a115bdbecbe9a5250017fc0#file-dynamicclass-ts
 * @see https://gist.github.com/buddhics/a29ca8ac4a115bdbecbe9a5250017fc0#file-dynamicclass-ts
 * @see https://medium.com/@buddhi.amigo/how-to-create-typescript-classes-dynamically-b29ca7767ee5
 */
export class InstanceLoader {
    static getInstance(className: string, ...args: any[]): any {
        if (Store[className] === undefined || Store[className] === null) {
            throw new Error(`Class type of \'${className}\' is not in the store`);
        }
        return new Store[className](args);
    }
}