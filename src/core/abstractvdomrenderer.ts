import {Message} from "@phosphor/messaging";

import {} from '@types/jquery';

import {VDomRenderer} from "@jupyterlab/apputils";

/**
 * Basic VDomRenderer of which model is null
 */
export abstract class AbstractVDomRenderer extends VDomRenderer<null>{

    /**
     * Initializing of the DetailsWidget when activating.
     */
    protected onActivateRequest(msg: Message) {
        super.onActivateRequest(msg);
        this.node.tabIndex = -1;
        this.node.focus();
    }

    /**
     * Dispose of the DetailsWidget when closing.
     */
    protected onCloseRequest(msg: Message): void {
        this.dispose();
    }

    /**
     * after initiating widget, this function will be fired.
     */
    public onReady(): void {

    }
}
